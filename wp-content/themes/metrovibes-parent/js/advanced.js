if (typenow === 'service')
    {
        jQuery.ajax({
        type: "POST",
        url: tf_script.ajaxurl,
        data: 'action=tfuse_ajax_get_service_link',
        success: function(msg){
            var obj = msg;
            if(obj == 'yes') 
            {
                jQuery('.metrovibes_single_image,.metrovibes_video_link,.metrovibes_disable_image,.metrovibes_disable_video,.metrovibes_disable_post_meta,.metrovibes_disable_published_date,.metrovibes_disable_coments,.metrovibes_disable_author_info,.metrovibes_disable_views ').hide();
                jQuery('.metrovibes_single_image,.metrovibes_disable_video,.metrovibes_disable_views').next().hide();
                jQuery('.metrovibes_service_custom_link').show();
            }
            else
            {
                jQuery('.metrovibes_single_image,.metrovibes_video_link,.metrovibes_disable_image,.metrovibes_disable_video,.metrovibes_disable_post_meta,.metrovibes_disable_published_date,.metrovibes_disable_coments,.metrovibes_disable_author_info,.metrovibes_disable_views ').show();
                jQuery('.metrovibes_single_image,.metrovibes_disable_video,.metrovibes_disable_views').next().show();
                jQuery('.metrovibes_service_custom_link').hide();
            }
        }
        });
    }

jQuery(document).ready(function($) {
    
    jQuery('.over_thumb ').bind('click', function(){
 
       window.setTimeout(function(){
           var sel = jQuery('#slider_design_type').val(); 
           if(sel == 'gallery' || sel == 'services'){
               jQuery('#slider_type').html('<option value="">Choose your slider type</option><option value="categories">Automatically, fetch images from categories</option><option value="posts">Automatically, fetch images from posts</option><option value="custom">Manually, I\'ll upload the images myself</option>');
            }
            else if(sel == 'onebyone'){
               jQuery('#slider_type').html('<option value="">Choose your slider type</option><option value="custom">Manually, I\'ll upload the images myself</option>');
            }
           else
            {
                jQuery('#slider_type').html('<option value="">Choose your slider type</option><option value="custom">Manually, I\'ll upload the images myself</option><option value="categories">Automatically, fetch images from categories</option><option value="posts">Automatically, fetch images from posts</option>');
            }
               
       },12);
    });
    
    if(typenow == 'portfolio')
    {

	$(".tf_images .tfuse-meta-radio-img-box:nth-child(1)").append('<div class="hover2">')
	$(".tf_images .tfuse-meta-radio-img-box:nth-child(1)").hover(function() {
		  $('.hover2').css({'background':'url(../wp-content/themes/metrovibes-parent/images/3columns_preview.png) no-repeat 0 0 ','position':'relative','z-index':'2','cursor':'pointer','width':'400px','height':'250px'});
		}, function() {
		  $('.hover2').removeAttr( 'style' );
	});
        $(".tf_images .tfuse-meta-radio-img-box:nth-child(2)").append('<div class="hover3">')
	$(".tf_images .tfuse-meta-radio-img-box:nth-child(2)").hover(function() {
		  $('.hover3').css({'background':'url(../wp-content/themes/metrovibes-parent/images/4columns_preview.png) no-repeat 0 0 ','position':'relative','z-index':'2','cursor':'pointer','width':'400px','height':'250px'});
		}, function() {
		  $('.hover3').removeAttr( 'style' );
	});
    }
    else
    {
		$("#portfolio.postbox .formcontainer .tfuse-meta-radio-img-box:nth-child(1)").append('<div class="hover2">')
		$("#portfolio.postbox .formcontainer .tfuse-meta-radio-img-box:nth-child(1)").hover(function() {
			  $('.hover2').css({'background':'url(../wp-content/themes/metrovibes-parent/images/3columns_preview.png) no-repeat 0 0 ','position':'relative','z-index':'2','cursor':'pointer','width':'400px','height':'250px'});
			}, function() {
			  $('.hover2').removeAttr( 'style' );
		});
			$("#portfolio.postbox .formcontainer .tfuse-meta-radio-img-box:nth-child(2)").append('<div class="hover3">')
		$("#portfolio.postbox .formcontainer .tfuse-meta-radio-img-box:nth-child(2)").hover(function() {
			  $('.hover3').css({'background':'url(../wp-content/themes/metrovibes-parent/images/4columns_preview.png) no-repeat 0 0 ','position':'relative','z-index':'2','cursor':'pointer','width':'400px','height':'250px'});
			}, function() {
			  $('.hover3').removeAttr( 'style' );
		});
    }
    
    if($('#metrovibes_dis_footer_social').is(':checked'))
        jQuery('.metrovibes_fb_url,.metrovibes_tweet_url,.metrovibes_dribble_url,.metrovibes_flickr_url,.metrovibes_feedburner_url,.metrovibes_feedburner_id').hide();
        
        $('#metrovibes_dis_footer_social').live('change',function () {
	if(jQuery(this).is(':checked'))
        {
            jQuery('.metrovibes_fb_url,.metrovibes_tweet_url,.metrovibes_dribble_url,.metrovibes_flickr_url,.metrovibes_feedburner_url,.metrovibes_feedburner_id').hide();
            jQuery('.metrovibes_dis_footer_social,.metrovibes_fb_url,.metrovibes_tweet_url,.metrovibes_dribble_url,.metrovibes_flickr_url,.metrovibes_feedburner_url').next().hide();
        }
	else
        {
            jQuery('.metrovibes_fb_url,.metrovibes_tweet_url,.metrovibes_dribble_url,.metrovibes_flickr_url,.metrovibes_feedburner_url,.metrovibes_feedburner_id').show();
            jQuery('.metrovibes_dis_footer_social,#metrovibes_dis_footer_social,.metrovibes_fb_url,.metrovibes_tweet_url,.metrovibes_dribble_url,.metrovibes_flickr_url,.metrovibes_feedburner_url').next().show();
        }
});

//hide header options if homepage_category  is different from tfuse_blog_posts or  tfuse_blog_cases
if(!$('#metrovibes_enable_footer_shortcodes').is(':checked')) jQuery('.metrovibes_footer_left_copyright,.metrovibes_footer_shortcodes').hide();
 	$('#metrovibes_enable_footer_shortcodes').live('change',function () {
	if(!jQuery(this).is(':checked'))
		jQuery('.metrovibes_footer_left_copyright,.metrovibes_footer_shortcodes').hide();
	else
		jQuery('.metrovibes_footer_left_copyright,.metrovibes_footer_shortcodes').show();
    });
    
 $('.tfuse_selectable_code').live('click', function () {
        var r = document.createRange();
        var w = $(this).get(0);
        r.selectNodeContents(w);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(r);
    });

    $('#tf_rf_form_name_select').change(function(){
        $_get=getUrlVars();
        if($(this).val()==-1 && 'formid' in $_get){
            delete $_get.formid;
        } else if($(this).val()!=-1){
            $_get.formid=$(this).val();
        }
        $_url_str='?';
        $.each($_get,function(key,val){
            $_url_str +=key+'='+val+'&';
        })
        $_url_str = $_url_str.substring(0,$_url_str.length-1);
        window.location.href=$_url_str;
    });


    function getUrlVars() {
        urlParams = {};
        var e,
            a = /\+/g,
            r = /([^&=]+)=?([^&]*)/g,
            d = function (s) {
                return decodeURIComponent(s.replace(a, " "));
            },
            q = window.location.search.substring(1);
        while (e = r.exec(q))
            urlParams[d(e[1])] = d(e[2]);
        return urlParams;
    }
	 $("#slider_slideSpeed,#slider_play,#slider_pause,#metrovibes_map_zoom").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

    jQuery('#metrovibes_map_lat,#metrovibes_map_long').keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 190 || event.keyCode == 110|| event.keyCode == 189 || event.keyCode == 109 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

    $('#metrovibes_framework_options_metabox .handlediv, #metrovibes_framework_options_metabox .hndle').hide();
    $('#metrovibes_framework_options_metabox .handlediv, #metrovibes_framework_options_metabox .hndle').hide();

    var options = new Array();
    
    options['metrovibes_footer_type'] = jQuery('#metrovibes_footer_type').val();
    jQuery('#metrovibes_footer_type').bind('change', function() {
        options['metrovibes_footer_type'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });
    
    options['metrovibes_header_type'] = jQuery('#metrovibes_header_type').val();
    jQuery('#metrovibes_header_type').bind('change', function() {
        options['metrovibes_header_type'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });
    
    options['metrovibes_homepage_category'] = jQuery('#metrovibes_homepage_category').val();
    jQuery('#metrovibes_homepage_category').bind('change', function() {
        options['metrovibes_homepage_category'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });
    
    options['slider_image_bg'] = jQuery('#slider_image_bg').val();
    jQuery('#slider_image_bg').bind('change', function() {
        options['slider_image_bg'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });

    options['metrovibes_type_post'] = jQuery('#metrovibes_type_post').val();
    jQuery('#metrovibes_type_post').bind('change', function() {
        options['metrovibes_type_post'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });
    
    options['metrovibes_type_post1'] = jQuery('#metrovibes_type_post1').val();
    jQuery('#metrovibes_type_post1').bind('change', function() {
        options['metrovibes_type_post1'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });

    options['metrovibes_header_element'] = jQuery('#metrovibes_header_element').val();
    jQuery('#metrovibes_header_element').bind('change', function() {
        options['metrovibes_header_element'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });
    
    options['metrovibes_header_element_posts'] = jQuery('#metrovibes_header_element_posts').val();
    jQuery('#metrovibes_header_element_posts').bind('change', function() {
        options['metrovibes_header_element_posts'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });
    
    options['metrovibes_header_element_portf'] = jQuery('#metrovibes_header_element_portf').val();
    jQuery('#metrovibes_header_element_portf').bind('change', function() {
        options['metrovibes_header_element_portf'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });
   
   options['metrovibes_choose_logo'] = jQuery('#metrovibes_choose_logo').val();
    jQuery('#metrovibes_choose_logo').bind('change', function() {
        options['metrovibes_choose_logo'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });
    
    options['metrovibes_page_title'] = jQuery('#metrovibes_page_title').val();
    jQuery('#metrovibes_page_title').bind('change', function() {
        options['metrovibes_page_title'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });

    options['slider_hoverPause'] = jQuery('#slider_hoverPause').val();
    jQuery('#slider_hoverPause').bind('change', function() {
       if (jQuery(this).next('.tf_checkbox_switch').hasClass('on'))  options['slider_hoverPause']= true;
        else  options['slider_hoverPause'] = false;
        tfuse_toggle_options(options);
    });

    options['map_type'] = jQuery('#metrovibes_map_type').val();
    jQuery(' #metrovibes_map_type').bind('change', function() {
        options['map_type'] = jQuery(this).val();
        tfuse_toggle_options(options);
    });
    
    //blog page
    options['metrovibes_blogpage_category'] = jQuery('#metrovibes_blogpage_category').val();
     jQuery('#metrovibes_blogpage_category').bind('change', function() {
         options['metrovibes_blogpage_category'] = jQuery(this).val();
         tfuse_toggle_options(options);
     });

     options['metrovibes_header_element_blog'] = jQuery('#metrovibes_header_element_blog').val();
     jQuery('#metrovibes_header_element_blog').bind('change', function() {
         options['metrovibes_header_element_blog'] = jQuery(this).val();
         tfuse_toggle_options(options);
     });
     options['metrovibes_before_content_element_blog'] = jQuery('#metrovibes_before_content_element_blog').val();
     jQuery('#metrovibes_before_content_element_blog').bind('change', function() {
         options['metrovibes_before_content_element_blog'] = jQuery(this).val();
         tfuse_toggle_options(options);
     });
     
      options['metrovibes_content_element'] = jQuery('#metrovibes_content_element').val();
     jQuery('#metrovibes_content_element').bind('change', function() {
         options['metrovibes_content_element'] = jQuery(this).val();
         tfuse_toggle_options(options);
     });
     
     options['posts_select_type'] = jQuery('#posts_select_type').val();
     jQuery('#posts_select_type').bind('change', function() {
         options['posts_select_type'] = jQuery(this).val();
         tfuse_toggle_options(options);
     });
     
     options['metrovibes_content_element_blog'] = jQuery('#metrovibes_content_element_blog').val();
     jQuery('#metrovibes_content_element_blog').bind('change', function() {
         options['metrovibes_content_element_blog'] = jQuery(this).val();
         tfuse_toggle_options(options);
     });
     
    tfuse_toggle_options(options);

    function tfuse_toggle_options(options)
    {

        jQuery('#metrovibes_footer_bg2,#metrovibes_footer_image_repeat,#metrovibes_header_image_repeat,#metrovibes_header_bg2,#metrovibes_header_image,#metrovibes_header_bg,#metrovibes_footer_image,#metrovibes_footer_image_repeat,#metrovibes_footer_bg,#metrovibes_header_video_blog,#metrovibes_header_subtitle_blog,#metrovibes_header_title_blog,#metrovibes_select_slider_blog,#metrovibes_page_map_blog,#metrovibes_header_email_blog,#metrovibes_header_phone_blog,#metrovibes_header_adress_blog,#metrovibes_header_video_posts,#metrovibes_header_subtitle_posts,#metrovibes_header_title_posts,#metrovibes_select_slider_posts,#metrovibes_page_map_posts,#metrovibes_header_email_posts,#metrovibes_header_phone_posts,#metrovibes_header_adress_posts,#metrovibes_header_video_portf,#metrovibes_header_subtitle_portf,#metrovibes_header_title_portf,#metrovibes_select_slider_portf,#metrovibes_page_map_portf,#metrovibes_header_email_portf,#metrovibes_header_phone_portf,#metrovibes_header_adress_portf,#metrovibes_header_video,#metrovibes_header_subtitle,#metrovibes_header_title,#metrovibes_select_slider,#metrovibes_header_email,#metrovibes_header_phone,#metrovibes_header_adress,#metrovibes_use_page_options,#metrovibes_page_map_blog,#metrovibes_home_page,#metrovibes_categories_select_categ,#metrovibes_page_map,#metrovibes_portf_audio,#metrovibes_portf_video,#metrovibes_slide_image,#metrovibes_logo_desc,#metrovibes_logo,#metrovibes_logo_text,.homepage_category_header_element').parents('.option-inner').hide();
        jQuery('#metrovibes_footer_bg2,#metrovibes_footer_image_repeat,#metrovibes_header_image_repeat,#metrovibes_header_bg2,#metrovibes_header_image,#metrovibes_header_bg,#metrovibes_footer_image,#metrovibes_footer_image_repeat,#metrovibes_footer_bg,#metrovibes_header_video_blog,#metrovibes_header_subtitle_blog,#metrovibes_header_title_blog,#metrovibes_select_slider_blog,#metrovibes_page_map_blog,#metrovibes_header_email_blog,#metrovibes_header_phone_blog,#metrovibes_header_adress_blog,#metrovibes_header_video_posts,#metrovibes_header_subtitle_posts,#metrovibes_header_title_posts,#metrovibes_select_slider_posts,#metrovibes_page_map_posts,#metrovibes_header_email_posts,#metrovibes_header_phone_posts,#metrovibes_header_adress_posts,#metrovibes_header_video_portf,#metrovibes_header_subtitle_portf,#metrovibes_header_title_portf,#metrovibes_select_slider_portf,#metrovibes_page_map_portf,#metrovibes_header_email_portf,#metrovibes_header_phone_portf,#metrovibes_header_adress_portf,#metrovibes_header_video,#metrovibes_header_subtitle,#metrovibes_header_title,#metrovibes_select_slider,#metrovibes_header_email,#metrovibes_header_phone,#metrovibes_header_adress,#metrovibes_use_page_options,#metrovibes_display_type_home,#metrovibes_page_map_blog,#metrovibes_home_page,#metrovibes_categories_select_categ,#metrovibes_page_map,#metrovibes_portf_audio,#metrovibes_portf_video,#metrovibes_slide_image,#metrovibes_logo_desc,#metrovibes_logo,#metrovibes_logo_text,.homepage_category_header_element').parents('.form-field').hide();
        jQuery('.slider_img_bg,.slider_color_bg,.metrovibes_slide_image,.metrovibes_display_type_home').hide();
        jQuery('.metrovibes_slide_image1').hide();
        
        
        //slider options
        if(options['slider_image_bg']=='custom'){
            jQuery('.slider_img_bg,.slider_color_bg').show();
        }

        if(options['metrovibes_footer_type'] == 'no')
        {
            jQuery('#metrovibes_footer_image,#metrovibes_footer_bg,#metrovibes_footer_image_repeat').parents('.option-inner').show();
            jQuery('#metrovibes_footer_image,#metrovibes_footer_bg,#metrovibes_footer_image_repeat').parents('.form-field').show();
        }
        else if(options['metrovibes_footer_type'] == 'gradient')
        {
            jQuery('#metrovibes_footer_bg,#metrovibes_footer_bg2').parents('.option-inner').show();
            jQuery('#metrovibes_footer_bg,#metrovibes_footer_bg2').parents('.form-field').show();
        }
        
        if(options['metrovibes_header_type'] == 'no')
        {
            jQuery('#metrovibes_header_image,#metrovibes_header_bg,#metrovibes_header_image_repeat').parents('.option-inner').show();
            jQuery('#metrovibes_header_image,#metrovibes_header_bg,#metrovibes_header_image_repeat').parents('.form-field').show();
        }
        else
        {
            jQuery('#metrovibes_header_bg,#metrovibes_header_bg2').parents('.option-inner').show();
            jQuery('#metrovibes_header_bg,#metrovibes_header_bg2').parents('.form-field').show();
        }
        
        
        /*--------------------------------------------------*/
        
        //homepage
       if(options['metrovibes_homepage_category']=='specific'){
            jQuery('.metrovibes_display_type_home').show();
            jQuery('.metrovibes_categories_select_categ').next().show();
            jQuery('#metrovibes_categories_select_categ').parents('.option-inner').show();
            jQuery('#metrovibes_categories_select_categ').parents('.form-field').show();
            jQuery('#metrovibes_content_top').parents('.option-inner').hide();
            jQuery('#metrovibes_content_top').parents('.form-field').hide();
           
            if($('#metrovibes_use_page_options').is(':checked')) 
                jQuery('#homepage-header,#homepage-banners,#homepage-shortcodes').removeAttr('style');
        }
        else if (options['metrovibes_homepage_category']=='all')
        {
            jQuery('#metrovibes_content_top').parents('.option-inner').hide();
            jQuery('#metrovibes_content_top').parents('.form-field').hide();
            jQuery('.metrovibes_display_type_home').show();
            jQuery('.metrovibes_categories_select_categ').next().show();
            if($('#metrovibes_use_page_options').is(':checked')) 
                jQuery('#homepage-header,#homepage-banners,#homepage-shortcodes').removeAttr('style');
        }
        else if(options['metrovibes_homepage_category']=='page'){
            jQuery('#metrovibes_content_top').parents('.option-inner').show();
            jQuery('#metrovibes_content_top').parents('.form-field').show();
            jQuery('#metrovibes_home_page,#metrovibes_use_page_options').parents('.option-inner').show();
            jQuery('#metrovibes_home_page,#metrovibes_use_page_options').parents('.form-field').show();
            jQuery('.metrovibes_categories_select_categ').next().hide();
            //use page options
            if($('#metrovibes_use_page_options').is(':checked')) jQuery('#homepage-header,#homepage-banners,#homepage-shortcodes').hide();
            $('#metrovibes_use_page_options').live('change',function () {
            if(jQuery(this).is(':checked'))
                    jQuery('#homepage-header,#homepage-banners,#homepage-shortcodes').hide();
            else
                    jQuery('#homepage-header,#homepage-banners,#homepage-shortcodes').show();
            });
        } 
        
        /*header element*/
        
        if (options['metrovibes_header_element'] == 'map')
        { 
            jQuery('#metrovibes_page_map,#metrovibes_header_email,#metrovibes_header_phone,#metrovibes_header_adress').parents('.option-inner').show();
            jQuery('#metrovibes_page_map,#metrovibes_header_email,#metrovibes_header_phone,#metrovibes_header_adress').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element'] == 'slider')
        { 
            jQuery('#metrovibes_select_slider').parents('.option-inner').show();
            jQuery('#metrovibes_select_slider').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element'] == 'title')
        { 
            jQuery('#metrovibes_header_subtitle,#metrovibes_header_title').parents('.option-inner').show();
            jQuery('#metrovibes_header_subtitle,#metrovibes_header_title').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element'] == 'video')
        { 
            jQuery('#metrovibes_header_video').parents('.option-inner').show();
            jQuery('#metrovibes_header_video').parents('.form-field').show();
        }
        
        //header elements for post type portfolio
        if (options['metrovibes_header_element_portf'] == 'map')
        { 
            jQuery('#metrovibes_page_map_portf,#metrovibes_header_email_portf,#metrovibes_header_phone_portf,#metrovibes_header_adress_portf').parents('.option-inner').show();
            jQuery('#metrovibes_page_map_portf,#metrovibes_header_email_portf,#metrovibes_header_phone_portf,#metrovibes_header_adress_portf').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element_portf'] == 'slider')
        { 
            jQuery('#metrovibes_select_slider_portf').parents('.option-inner').show();
            jQuery('#metrovibes_select_slider_portf').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element_portf'] == 'title')
        { 
            jQuery('#metrovibes_header_subtitle_portf,#metrovibes_header_title_portf').parents('.option-inner').show();
            jQuery('#metrovibes_header_subtitle_portf,#metrovibes_header_title_portf').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element_portf'] == 'video')
        { 
            jQuery('#metrovibes_header_video_portf').parents('.option-inner').show();
            jQuery('#metrovibes_header_video_portf').parents('.form-field').show();
        }
        
        //header elements for post type post
        if (options['metrovibes_header_element_posts'] == 'map')
        { 
            jQuery('#metrovibes_page_map_posts,#metrovibes_header_email_posts,#metrovibes_header_phone_posts,#metrovibes_header_adress_posts').parents('.option-inner').show();
            jQuery('#metrovibes_page_map_posts,#metrovibes_header_email_posts,#metrovibes_header_phone_posts,#metrovibes_header_adress_posts').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element_posts'] == 'slider')
        { 
            jQuery('#metrovibes_select_slider_posts').parents('.option-inner').show();
            jQuery('#metrovibes_select_slider_posts').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element_posts'] == 'title')
        { 
            jQuery('#metrovibes_header_subtitle_posts,#metrovibes_header_title_posts').parents('.option-inner').show();
            jQuery('#metrovibes_header_subtitle_posts,#metrovibes_header_title_posts').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element_posts'] == 'video')
        { 
            jQuery('#metrovibes_header_video_posts').parents('.option-inner').show();
            jQuery('#metrovibes_header_video_posts').parents('.form-field').show();
        }
        
        //header elements for blog page
        if (options['metrovibes_header_element_blog'] == 'map')
        { 
            jQuery('#metrovibes_page_map_blog,#metrovibes_header_email_blog,#metrovibes_header_phone_blog,#metrovibes_header_adress_blog').parents('.option-inner').show();
            jQuery('#metrovibes_page_map_blog,#metrovibes_header_email_blog,#metrovibes_header_phone_blog,#metrovibes_header_adress_blog').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element_blog'] == 'slider')
        { 
            jQuery('#metrovibes_select_slider_blog').parents('.option-inner').show();
            jQuery('#metrovibes_select_slider_blog').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element_blog'] == 'title')
        { 
            jQuery('#metrovibes_header_subtitle_blog,#metrovibes_header_title_blog').parents('.option-inner').show();
            jQuery('#metrovibes_header_subtitle_blog,#metrovibes_header_title_blog').parents('.form-field').show();
        }
        else if (options['metrovibes_header_element_blog'] == 'video')
        { 
            jQuery('#metrovibes_header_video_blog').parents('.option-inner').show();
            jQuery('#metrovibes_header_video_blog').parents('.form-field').show();
        }
        
        //blog page
        if(options['metrovibes_blogpage_category']=='all'){
            jQuery('.metrovibes_categories_select_categ_blog').hide();
        }
        else if(options['metrovibes_blogpage_category']=='specific'){
            jQuery('.metrovibes_categories_select_categ_blog').show();
        } 
        
        
         if (options['metrovibes_header_element_blog'] == 'map')
        {
            jQuery('#metrovibes_page_map_blog,#metrovibes_header_name_blog,#metrovibes_header_email_blog,#metrovibes_header_phone_blog,#metrovibes_header_adress_blog').parents('.option-inner').show();
            jQuery('#metrovibes_page_map_blog,#metrovibes_header_name_blog,#metrovibes_header_email_blog,#metrovibes_header_phone_blog,#metrovibes_header_adress_blog').parents('.form-field').show();
        }
        
        
       //header 
        var homepage = true;
        if (jQuery('.homepage_category_header_element').length == 1) homepage = false;
        if ( options['metrovibes_homepage_category'] == 'tfuse_blog_posts' || options['metrovibes_homepage_category'] == 'tfuse_blog_cases')
        {
            homepage = true;
            jQuery('.homepage_category_header_element').parents('.option-inner').show();
            jQuery('.homepage_category_header_element').parents('.form-field').show();
        }
 
        //logo type
        if(options['metrovibes_choose_logo'] == 'text')
        { 
            jQuery('#metrovibes_logo_text').parents('.option-inner').show();
            jQuery('#metrovibes_logo_text').parents('.form-field').show();
            jQuery('#metrovibes_logo_desc').parents('.option-inner').show();
            jQuery('#metrovibes_logo_desc').parents('.form-field').show();
        }
        else
        {
            jQuery('#metrovibes_logo').parents('.option-inner').show();
            jQuery('#metrovibes_logo').parents('.form-field').show();
        }
        //hide page title
        if(options['metrovibes_page_title'] == 'custom_title')
        { 
            jQuery('#metrovibes_custom_title').parents('.option-inner').show();
            jQuery('#metrovibes_custom_title').parents('.form-field').show();
        }
		else
        { 
            jQuery('#metrovibes_custom_title').parents('.option-inner').hide();
            jQuery('#metrovibes_custom_title').parents('.form-field').hide();
        }
        //slider
        if (options['slider_hoverPause'])
        {
            jQuery('.slider_pause').show();
            jQuery('.slider_pause').next('.tfclear').show();
        }
        else
        {
            jQuery('.slider_pause').hide();
            jQuery('.slider_pause').next('.tfclear').hide();
        }

        if ( (options['map_type'] == 'map3') && (options['metrovibes_header_element'] == 'map') && homepage)
        {
            jQuery('#metrovibes_map_address').parents('.option-inner').show();
            jQuery('#metrovibes_map_address').parents('.form-field').show();
        }
    }
});