<?php get_header(); $view = tfuse_archive_view();?>
<?php include_once 'portfolio-navigation.php'; ?>
<div id="middle" class="full_width2">
    <div class="container">
        <div class="content" role="main" >
                <article class="post-detail">            
                    <div class="entry clearfix">
                        <div class="gallery-list postajax clearfix <?php echo $view;?>" <?php tfuse_more_pagination();?>>
                            <?php if (have_posts()) 
                             { $count = 0; 
                                 while (have_posts()) : the_post(); $count++;
                                      get_template_part('listing', 'portfolio');
                                 endwhile;
                             } 
                             else 
                             { ?>
                                 <h5><?php _e('Sorry, no posts matched your criteria.', 'tfuse'); ?></h5>
                          <?php } ?>
                        </div>
                    </div>
                </article>
              <?php tfuse_load_pagination();?>
              <?php tfuse_choose_pagination();?>
        </div>
    </div> 
</div><!--/ .middle -->
 <div class="clear"></div>
 <script type="text/javascript">
	jQuery(document).ready(function(){ 
		jQuery(".preload").preloadify({ imagedelay:100 });
	});
</script>
<div class="middle-bottom">
    <div class="container">
        <?php tfuse_shortcode_content('after'); ?>
    </div>
</div>
<?php  $id = tfuse_get_cat_id();?>
<input type="hidden" value="<?php echo $id; ?>" name="current_cat"  />
<input type="hidden" value="yes" name="is_this_tax"  />
<?php get_footer();?>