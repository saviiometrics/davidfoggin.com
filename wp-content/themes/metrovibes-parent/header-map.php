<?php
$template_directory = get_template_directory_uri();
wp_register_script('maps.google.com', 'http://maps.google.com/maps/api/js?sensor=false', array('jquery'), '', true);
wp_register_script('jquery.gmap', $template_directory . '/js/jquery.gmap.min.js', array('jquery', 'maps.google.com'), '', true);
wp_print_scripts('maps.google.com');
wp_print_scripts('jquery.gmap');

global $is_tf_blog_page,$header_map,$TFUSE;
$posttype = $post->post_type; 
$filter =  $TFUSE->request->isset_GET('posts') ? $TFUSE->request->GET('posts') : "";
if ( $posttype == 'post' && $filter == 'all')
{
    //if is page
    $tmp_conf ['show_all_markers'] = false;
    $coords = explode(':', tfuse_options('page_map_posts'));
    if((!$coords[0]) || (!$coords[1]))
    {
        $tmp_conf ['show_all_markers'] = true;
    }
    else
    {
        $tmp_conf['post_coords']['lat']     = preg_replace('[^0-9\.]', '', $coords[0]);
        $tmp_conf['post_coords']['lng']     = preg_replace('[^0-9\.]', '', $coords[1]);

        $tmp_conf['post_coords']['html']    = '<strong>'.__('We','tfuse').'</strong><span>'.__('are','tfuse').'</span>'.__('here','tfuse');
    }
}
elseif ( $posttype == 'portfolio' && $filter == 'all')
{
    //if is page
    $tmp_conf ['show_all_markers'] = false;
    $coords = explode(':', tfuse_options('page_map_portf'));
    if((!$coords[0]) || (!$coords[1]))
    {
        $tmp_conf ['show_all_markers'] = true;
    }
    else
    {
        $tmp_conf['post_coords']['lat']     = preg_replace('[^0-9\.]', '', $coords[0]);
        $tmp_conf['post_coords']['lng']     = preg_replace('[^0-9\.]', '', $coords[1]);

        $tmp_conf['post_coords']['html']    = '<strong>'.__('We','tfuse').'</strong><span>'.__('are','tfuse').'</span>'.__('here','tfuse');
    }
}
elseif ( $is_tf_blog_page )
{
    //if is blog page
    $tmp_conf['post_id'] = $post->ID;
    $tmp_conf ['show_all_markers'] = false;
    $coords = explode(':', tfuse_options('page_map_blog'));
    if((!$coords[0]) || (!$coords[1]))
    {
        $tmp_conf ['show_all_markers'] = true;
    }
    else
    {
        $tmp_conf['post_coords']['lat']     = preg_replace('[^0-9\.]', '', $coords[0]);
        $tmp_conf['post_coords']['lng']     = preg_replace('[^0-9\.]', '', $coords[1]);

        $tmp_conf['post_coords']['html']    = '<strong>'.__('We','tfuse').'</strong><span>'.__('are','tfuse').'</span>'.__('here','tfuse');
    }
}
elseif (is_front_page())
{
    $page_id = tfuse_options('home_page');
    if(tfuse_options('use_page_options') && tfuse_options('homepage_category')=='page')
    {   
        $tmp_conf['post_id'] = $page_id;
         $tmp_conf ['show_all_markers'] = false;
        $coords = explode(':', tfuse_page_options('page_map','',$page_id)); 
    }
    else
    {   
        $tmp_conf['post_id'] = $post->ID;
         $tmp_conf ['show_all_markers'] = false;
        $coords = explode(':', tfuse_options('page_map'));
    }
     if((!$coords[0]) || (!$coords[1]))
        {
            $tmp_conf ['show_all_markers'] = true;
        }
        else
        {
            $tmp_conf['post_coords']['lat']     = preg_replace('[^0-9\.]', '', $coords[0]);
            $tmp_conf['post_coords']['lng']     = preg_replace('[^0-9\.]', '', $coords[1]);

            $tmp_conf['post_coords']['html']    = '<strong>'.__('We','tfuse').'</strong><span>'.__('are','tfuse').'</span>'.__('here','tfuse');
        }
}
elseif (is_category())
{
    //if is front_page
    $ID = get_query_var('cat'); 
    $tmp_conf ['show_all_markers'] = false;
    $coords = explode(':', tfuse_options('page_map',null,$ID));
    if((!$coords[0]) || (!$coords[1]))
    {
        $tmp_conf ['show_all_markers'] = true;
    }
    else
    {
        $tmp_conf['post_coords']['lat']     = preg_replace('[^0-9\.]', '', $coords[0]);
        $tmp_conf['post_coords']['lng']     = preg_replace('[^0-9\.]', '', $coords[1]);

        $tmp_conf['post_coords']['html']    = '<strong>'.__('We','tfuse').'</strong><span>'.__('are','tfuse').'</span>'.__('here','tfuse');
    }
}
elseif (is_tax())
{
    $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
    $ID = $term->term_id;
    $tmp_conf ['show_all_markers'] = false;
    $coords = explode(':', tfuse_options('page_map',null,$ID));
    if((!$coords[0]) || (!$coords[1]))
    {
        $tmp_conf ['show_all_markers'] = true;
    }
    else
    {
        $tmp_conf['post_coords']['lat']     = preg_replace('[^0-9\.]', '', $coords[0]);
        $tmp_conf['post_coords']['lng']     = preg_replace('[^0-9\.]', '', $coords[1]);

        $tmp_conf['post_coords']['html']    = '<strong>'.__('We','tfuse').'</strong><span>'.__('are','tfuse').'</span>'.__('here','tfuse');
    }
}
elseif ((is_page() || is_single()))
{
    //if is page
    $tmp_conf['post_id'] = $post->ID;
    $tmp_conf ['show_all_markers'] = false;
    $coords = explode(':', tfuse_page_options('page_map'));
    if((!$coords[0]) || (!$coords[1]))
    {
        $tmp_conf ['show_all_markers'] = true;
    }
    else
    {
        $tmp_conf['post_coords']['lat']     = preg_replace('[^0-9\.]', '', $coords[0]);
        $tmp_conf['post_coords']['lng']     = preg_replace('[^0-9\.]', '', $coords[1]);

        $tmp_conf['post_coords']['html']    = '<strong>'.__('We','tfuse').'</strong><span>'.__('are','tfuse').'</span>'.__('here','tfuse');
    }
}



if(!empty($tmp_conf['post_coords']['lat']) || !empty($tmp_conf['post_coords']['lng'])):
?>
		    
<script>
    jQuery(window).ready(function () {
    jQuery("#map1").gMap({
            scrollwheel: false,
            markers: [{
            latitude: <?php echo $tmp_conf['post_coords']['lat']; ?>,
            longitude: <?php echo  $tmp_conf['post_coords']['lng'];?>,
            popup: false}],
            zoom: 16
            });
    });
</script> 
<!-- header -->
<div class="header header-map" >
    <div class="container">
        <div id="map1" class="map frame_box" style="width: 100%; box-shadow: none;margin-top: -7px;  overflow: hidden;"> </div>
    </div>
</div>
<!--/ header -->
<?php if(!empty($header_map['phone']) || !empty($header_map['email']) || !empty($header_map['adress'])):?>
<!--header bottom-->
<div class="header-bottom">
    <div class="container clearfix">
       <div class="contact_list clearfix ">
            <ul class="contact_items"> 
                <?php if(!empty($header_map['adress'])):?>
                    <li><i class="location-ico"></i><span><?php echo $header_map['adress'];?></span></li>
                <?php endif;?>
                <?php if(!empty($header_map['email'])):?>
                    <li><i class="mail-ico"></i><span><a href="mailto:<?php echo $header_map['email'];?>" class="mailhref"><?php echo $header_map['email'];?></a></span></li>
                <?php endif;?>
                <?php if(!empty($header_map['phone'])):?>
                    <li><i class="phone-ico"></i><span><?php echo $header_map['phone'];?></span></li>
                <?php endif;?>
            </ul>
        </div>
    </div>
</div>
<!--/header bottom-->
<?php endif;?>
<?php endif;?>
