<?php get_header(); ?>
<?php $sidebar_position = tfuse_sidebar_position(); ?>
<div class="header-bottom">
    <div class="container">
        <div class="header-title">
            <h1><?php tfuse_cat_names() ;?></h1>
        </div>
    <div class="clear"></div>
    </div>
</div>
<?php if ($sidebar_position == 'right') : ?>
         <div id="middle" class="cols2">
<?php endif;
    if ($sidebar_position == 'left') : ?>
         <div id="middle" class="cols2 sidebar_left">
<?php endif;
     if ($sidebar_position == 'full') : ?>
          <div id="middle" class="full_width">
<?php endif; ?> 
    <div class="container">
        <!-- content --> 
        <div class="content" role="main">
            <article class="post-item post-detail clearfix"> 
                <?php  while ( have_posts() ) : the_post();?>
                        <?php get_template_part('content','single');?>
                <?php endwhile; // end of the loop. ?> 
            </article>
             <div class="clear"></div>
                <?php if ( tfuse_page_options('disable_comments',tfuse_options('disable_posts_comments')) ) : ?>
                    <?php  tfuse_comments(); ?>
                <?php endif; ?>
        </div>
        <!--/ content --> 
            <?php if (($sidebar_position == 'right') || ($sidebar_position == 'left')) : ?>
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div><!--/ .sidebar -->
            <?php endif; ?>
    </div> 
</div><!--/ .middle -->
 <div class="clear"></div>
 <div class="middle-bottom">
    <div class="container">
        <?php tfuse_shortcode_content('after'); ?>
    </div>
</div>
<?php get_footer();?>