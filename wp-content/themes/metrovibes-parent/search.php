<?php get_header(); $search_query = get_search_query();?>
<?php $sidebar_position = tfuse_sidebar_position(); ?>
<?php if ($sidebar_position == 'right') : ?>
         <div id="middle" class="cols2">
<?php endif;
    if ($sidebar_position == 'left') : ?>
         <div id="middle" class="cols2 sidebar_left">
<?php endif;
     if ($sidebar_position == 'full') : ?>
          <div id="middle" class="full_width">
<?php endif; ?> 
    <div class="container">
        <div class="content" role="main" <?php tfuse_more_pagination();?>>
            <div class="postlist postview clearfix search">
           <?php if (have_posts()) 
            { $count = 0;
                while (have_posts()) : the_post(); $count++;
                    get_template_part('listing', 'blog');
                endwhile;
            } 
            else 
            { ?>
                <h5><?php _e('Sorry, no posts matched your criteria.', 'tfuse'); ?></h5>
      <?php } ?>
            </div>
           <?php tfuse_choose_pagination();?>
        </div>
            <?php if (($sidebar_position == 'right') || ($sidebar_position == 'left')) : ?>
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div><!--/ .sidebar -->
            <?php endif; ?>
    </div> 
</div><!--/ .middle -->
<div class="clear"></div>
</div>
<input type="hidden" value="<?php echo $search_query; ?>" name="search_key"  />
<?php get_footer();?>
