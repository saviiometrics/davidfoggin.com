<?php 
    global $header_video;
    if ( !empty($header_video['video'])) :
        $video = '';
         preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $header_video['video'], $video_id);
         if(!empty($video_id)) $video .= '<iframe src="http://www.youtube.com/embed/'.$video_id[0].'?wmode=transparent" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
         else $video = '<iframe src="'.$header_video['video'].'" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
?>
<div class="header" <?php echo tfuse_change_header_bg();?>>
    <div class="header_inner">
        <div class="container">
            <div class="header_video">
               <?php echo $video;?> 
            </div>
        </div>
    </div>
</div>
<?php endif;?>