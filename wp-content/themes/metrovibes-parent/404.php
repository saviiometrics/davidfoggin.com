<?php get_header(); ?>
<?php $sidebar_position = tfuse_sidebar_position(); ?>
<?php if ($sidebar_position == 'right') : ?>
         <div id="middle" class="cols2">
<?php endif;
    if ($sidebar_position == 'left') : ?>
         <div id="middle" class="cols2 sidebar_left">
<?php endif;
     if ($sidebar_position == 'full') : ?>
          <div id="middle" class="full_width">
<?php endif; ?> 
    <div class="container">
        <div class="content" role="main">
            <article class="post-details">  
                <div class="entry clearfix">
                    <p><?php _e('Page not found', 'tfuse') ?></p>
                    <p><?php _e('The page you were looking for doesn&rsquo;t seem to exist', 'tfuse') ?>.</p>
                </div><!--/ .entry -->
            </article>
            <div class="clear"></div>
        </div>            
        <?php if (($sidebar_position == 'right') || ($sidebar_position == 'left')) : ?>
            <div class="sidebar">
                <?php get_sidebar(); ?>
            </div><!--/ .sidebar -->
        <?php endif; ?>
    </div> 
</div><!--/ .middle -->
 <div class="clear"></div>
<?php get_footer();?>