<?php
class TFuse_Widget_Testimonial extends WP_Widget {

    function TFuse_Widget_Testimonial()
    {
        $widget_ops = array('classname' => '', 'description' => __("Display and rotate your testimonials","tfuse"));
        $this->WP_Widget('testimonial', __('TFuse - Testimonial','tfuse'), $widget_ops);
    }

    function widget( $args, $instance ) {
        extract($args);
		$testimonials_uniq = rand(1, 300);
        $title = apply_filters( 'widget_title',  empty($instance['title']) ? __('Testimonial','tfuse') : $instance['title'], $instance, $this->id_base);

        $slide = $nav = $single = '';
        query_posts('post_type=testimonials&posts_per_page=-1&order=ASC');
        $k = 0;
        if (have_posts()) {
        while (have_posts()) {
           
            $k++;
            the_post();
            $positions = '';
            $terms = get_the_terms(get_the_ID(), 'testimonials');
                
            if (!is_wp_error($terms) && !empty($terms))
                foreach ($terms as $term)
                    $positions .= ', ' . $term->name;
            $slide .= '
                <div class="slider-item">
                    <div class="quote-text">
                    '.get_the_excerpt() . '
                        <span class="quote-author">
                            ' . get_the_title() . '
                        </span>
                    </div>
                </div>
        ';
        } // End WHILE Loop
    } // End IF Statement
    wp_reset_query();

    $output = '
     <div class="widget-container slider slider_quotes"><h3>'.$title.'</h3>
     <div class="slider_wrapper">
      <div class="slider_container clearfix" id="testimonials'.$testimonials_uniq.'">
        ' . $slide . '
    </div></div>
    <script>
                jQuery(document).ready(function($) {
                    jQuery("#testimonials'.$testimonials_uniq.'").carouFredSel({
                        next : null,
                        prev : null,
                        infinite: false,
                        items: 1,
                        auto: {
                            play: true,
                            timeoutDuration: 15000
                        },
                        scroll: {
                            items : 1,
                            fx: "crossfade",
                            easing: "linear",
                            pauseOnHover: true,
                            duration: 400
                        }
                    });
                });
            </script>  ';

    echo $output;

    }
    function update($new_instance, $old_instance)
    { $instance = $old_instance;
        $instance['random'] = isset($new_instance['random']);
        $instance['title'] = $new_instance['title'];
        return $instance;

    } // End function update

    function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'random' => '') );
        $title = $instance['title'];

        ?>

    <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title (appears in footer):','tfuse'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
        
         <?php
       }

}
register_widget('TFuse_Widget_Testimonial'); ?>
