<?php

/**
 * Testimonials
 * 
 * To override this shortcode in a child theme, copy this file to your child theme's
 * theme_config/extensions/shortcodes/shortcodes/ folder.
 * 
 * Optional arguments:
 * title:
 * order: RAND, ASC, DESC
 */
function tfuse_testimonials($atts, $content = null) {
    global $testimonials_uniq;
    extract(shortcode_atts(array( 'type'=>'','number' => '','title1'=>'','boxcol'=>'','titlecol'=>'','img'=>''), $atts));
    
    $slide = $nav = $single = '';$sidebar_position = tfuse_sidebar_position();
    $sidebar_position = tfuse_sidebar_position();
	$title = '';
    $testimonials_uniq = rand(1, 300);

    $posts = get_posts('post_type=testimonials&posts_per_page=-1&order=DESC');
    $k = 0;
    if($type == 'type2')
    { $k = 0;
        foreach($posts as $post){
           if($k == $number) break; $k++;
            $id = get_the_ID();
            $positions = '';
            $terms = get_the_terms(get_the_ID(), 'testimonials');
                
            if (!is_wp_error($terms) && !empty($terms))
                foreach ($terms as $term)
                    $positions .= ', ' . $term->name;
            $slide .= '
                 <div class="slider-item">
                    <div class="quote-text">
                    ' .strip_tags(apply_filters('the_content',$post->post_content)). '
                        <span class="quote-author">
                        ' . $post->post_title . '
                        </span>
                    </div>
                 </div>
        ';
    } // End IF Statement

    $output = '<div class="widget-container slider slider_quotes">
        <div class="slider_wrapper">
        <div class="slider_container clearfix" id="testimonials'.$testimonials_uniq.'">
            '.$slide.'
        </div>
        </div></div>
        <script>
                    jQuery(document).ready(function($) {
                        $("#testimonials'.$testimonials_uniq.'").carouFredSel({
                            next : null,
                            prev : null,
                            infinite: false,
                            items: 1,
                            auto: {
                                play: true,
                                timeoutDuration: 15000
                            },
                            scroll: {
                                items : 1,
                                fx: "crossfade",
                                easing: "linear",
                                pauseOnHover: true,
                                duration: 400
                            }
                        });
                    });
                </script>';
    }
    elseif($type == 'type3') 
    {  $k == 0;
        foreach($posts as $post){
            if($k == $number) break; $k++;
            $id = get_the_ID();
            $img = tfuse_page_options('img','',$post->ID);
            $positions = '';
            $terms = get_the_terms(get_the_ID(), 'testimonials');
                
            if (!is_wp_error($terms) && !empty($terms))
                foreach ($terms as $term)
                    $positions .= ', ' . $term->name;
            if(!empty($img)) $img = '<img src="'.$img .'"  alt="">';
            $slide .= '
                 <li>
                 <div class="slider-item clearfix">
                    <div class="quote-image">'.$img.'</div>
                        <div class="slider_wrapper">
                            <div class="quote-text clearfix">'.strip_tags(apply_filters('the_content',$post->post_content)).'
                                <span class="quote-author"></span>
                            </div>
                        </div>
                </div>
                 </li>
        ';
    } // End IF Statement

    $output = '<div class="header_testimonials" >
        <ul class="testimonials_slider'.$testimonials_uniq.'">
            '.$slide.'
                </ul>
        </div>
        <script>
            jQuery(document).ready(function(){
                $(".testimonials_slider'.$testimonials_uniq.'").bxSlider({
                    auto: true,
                    easing: "ease",
                    speed: 200,
                    adaptiveHeight: true,
                    mode:"fade",
                    pager:false,
                    controls:false,
                    autoControls:false
                });
            });
        </script>';
    }
    elseif($type == 'type1')
    { $k == 0;
        foreach($posts as $post){
            if($k == $number) break; $k++;
            $id = get_the_ID();
            $img = tfuse_page_options('img','',$post->ID);
            $align = tfuse_page_options('test_img_position','',$post->ID);
            $positions = '';
            $terms = get_the_terms(get_the_ID(), 'testimonials');
            if($align == 'alignright') $right = 'right'; else $right = '';               
            if (!is_wp_error($terms) && !empty($terms))
                foreach ($terms as $term)
                    $positions .= ', ' . $term->name;
            if(!empty($img)) $img = '<img src="'.$img .'"  alt="">';
            $slide .= '
                 <div class="widget-container widget-testimonials img-to-center clearfix '.$right.'">
                    <div class="testimonials-img">'.$img.'</div>
                        <div class="slider_wrapper">
                            <div class="testimonials-text clearfix">'.strip_tags(apply_filters('the_content',$post->post_content)).'
                               <span class="testimonials-author">' . $post->post_title . '</span>
                            </div>
                        </div>
                </div>
        ';
    } // End IF Statement

    $output = '<div class="testimonials-list">
            '.$slide.'
        </div>
        ';
    }
    elseif($type == 'type4') 
    {  $k == 0;
        foreach($posts as $post){
            if($k == $number) break; $k++;
            $id = get_the_ID();
            $positions = '';
            $terms = get_the_terms(get_the_ID(), 'testimonials');
                
            if (!is_wp_error($terms) && !empty($terms))
                foreach ($terms as $term)
                    $positions .= ', ' . $term->name;
            $slide .= '
                 <div class="slider-item">
                        <div class="slider_wrapper">
                            <div class="quote-text">'.strip_tags(apply_filters('the_content',$post->post_content)).'
                                <span class="quote-author"> ' . $post->post_title . '</span>
                            </div>
                        </div>
                </div>
        ';
    } // End IF Statement

    $output = '<div class="widget-container slider slider_quotes " style="background:'.$boxcol.'">
        <h3 style="color:'.$titlecol.'"><img src="'.$img.'" alt="">'.$title1.'</h3>
        <div class="slider_wrapper">
         <div class="slider_container clearfix" id="testimonials'.$testimonials_uniq.'">
            '.$slide.'
                </div>
        </div></div>
        <script>
                     jQuery(document).ready(function($) {
                         $("#testimonials'.$testimonials_uniq.'").carouFredSel({
                             next : null,
                             prev : null,
                             infinite: false,
                             items: 1,
                             auto: {
                                 play: true,
                                 timeoutDuration: 15000
                             },
                             scroll: {
                                 items : 1,
                                 fx: "crossfade",
                                 easing: "linear",
                                 pauseOnHover: true,
                                 duration: 400
                             }
                         });
                     });
                 </script>';
    }
    return $output;
}

$atts = array(
    'name' => __('Testimonials', 'tfuse'),
    'desc' => __('Here comes some lorem ipsum description for the box shortcode.', 'tfuse'),
    'category' => 11,
    'options' => array(
        array(
            'name' => __('Type', 'tfuse'),
            'desc' => __('Select display type', 'tfuse'),
            'id' => 'tf_shc_testimonials_type',
            'value' => 'type4',
            'options' => array(
                'type1' => __('With image', 'tfuse'),
                'type2' => 'Slider Without Image',
                'type3' => 'Slider With image',
                'type4' => __('Slider', 'tfuse')
            ),
            'type' => 'select'
        ),
        array(
            'name' => __('Number of items', 'tfuse'),
            'desc' => __('Enter the number of items', 'tfuse'),
            'id' => 'tf_shc_testimonials_number',
            'value' => '6',
            'type' => 'text'
        ),
        array(
            'name' => __('Title', 'tfuse'),
            'desc' => __('Enter the title', 'tfuse'),
            'id' => 'tf_shc_testimonials_title1',
            'value' => 'Clients',
            'type' => 'text'
        ),
        array(
            'name' => __('Box color', 'tfuse'),
            'desc' => __('Pick box color', 'tfuse'),
            'id' => 'tf_shc_testimonials_boxcol',
            'value' => '',
            'type' => 'colorpicker'
        ),
        array(
            'name' => __('Title color', 'tfuse'),
            'desc' => __('Pick title color', 'tfuse'),
            'id' => 'tf_shc_testimonials_titlecol',
            'value' => '',
            'type' => 'colorpicker'
        ),
        array(
            'name' => __('Image', 'tfuse'),
            'desc' => __('URL of an image', 'tfuse'),
            'id' => 'tf_shc_testimonials_img',
            'value' => '',
            'type' => 'text'
        )
    )
);

tf_add_shortcode('testimonials', 'tfuse_testimonials', $atts);
