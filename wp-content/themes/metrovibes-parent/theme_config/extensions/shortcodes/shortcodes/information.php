<?php

function tfuse_information($atts, $content = null) {

    //extract short code attr
    extract(shortcode_atts(array(
        'title' => '',
        'link' => '',
        'img' => '',
    ), $atts));

    $return_html = '<div class="about-item clearfix">
                        <div class="about-image"><img src="'.$img.'" alt=""></div>
                        <div class="about-title"><h2><a href="'.$link.'">'.$title.'</a></h2></div>
                        <div class="about-desc clearfix">
                                <p>'.html_entity_decode(do_shortcode($content)).'</p>
                        </div>
                    </div>';

    return $return_html;
}

$atts = array(
    'name' => __('Information', 'tfuse'),
    'desc' => __('Here comes some lorem ipsum description for the box shortcode.', 'tfuse'),
    'category' => 7,
    'options' => array(
        array(
            'name' => __('Title', 'tfuse'),
            'desc' => __('Give a title', 'tfuse'),
            'id' => 'tf_shc_information_title',
            'value' => '',
            'type' => 'text'
        ),
        array(
            'name' => __('Link', 'tfuse'),
            'desc' => __('Specify the target link ', 'tfuse'),
            'id' => 'tf_shc_information_link',
            'value' => '',
            'type' => 'text'
        ),
        array(
            'name' => __('Image', 'tfuse'),
            'desc' => __('Specify the image source', 'tfuse'),
            'id' => 'tf_shc_information_img',
            'value' => '',
            'type' => 'text'
        ),
        /* add the fllowing option in case shortcode has content */
        array(
            'name' => __('Content', 'tfuse'),
            'desc' => __('Enter shortcode content', 'tfuse'),
            'id' => 'tf_shc_information_content',
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'type' => 'textarea'
        )
    )
);

tf_add_shortcode('information', 'tfuse_information', $atts);

?>