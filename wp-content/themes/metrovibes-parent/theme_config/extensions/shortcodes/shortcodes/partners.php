<?php
function tfuse_partners($atts, $content = null) {
    global $partner;
    extract(shortcode_atts(array('title' => ''), $atts));
    $partner = '';
    $get_partners = do_shortcode($content);
    if (empty($title))
        $title = '';
    else
        $title = '<h2>' . $title . '</h2>';
    $i = 0;
    $output = '';
    $output .= '<div class="partener">
                   '.$title.'
                    <ul class="partnerships clearfix">';

    while (isset($partner['src'][$i])) {
        $output .= '<li><a href="'.$partner['link'][$i].'"><img src="'.$partner['src'][$i].'" alt=""></a></li>';
    $i++;
        
    }
    $output .= '</ul></div>';
    return $output;
}

$atts = array(
    'name' => __('Partners', 'tfuse'),
    'desc' => __('Here comes some lorem ipsum description for the box shortcode.', 'tfuse'),
    'category' => 4,
    'options' => array(
        array(
            'name' => __('Title', 'tfuse'),
            'desc' => '',
            'id' => 'tf_shc_partners_title',
            'value' => '',
            'type' => 'text',
            'divider' => true
        ),
        array(
            'name' => __('Source', 'tfuse'),
            'desc' => '',
            'id' => 'tf_shc_partners_src',
            'value' => '',
            'properties' => array('class' => 'tf_shc_addable_0 tf_shc_addable'),
            'type' => 'text'
        ),
        array(
            'name' => __('Link', 'tfuse'),
            'desc' => '',
            'id' => 'tf_shc_partners_link',
            'value' => '',
            'properties' => array('class' => 'tf_shc_addable_1 tf_shc_addable tf_shc_addable_last'),
            'type' => 'text'
        )

    )
);

tf_add_shortcode('partners', 'tfuse_partners', $atts);


function tfuse_partner($atts, $content = null)
{
    global $partner;
    extract(shortcode_atts(array('src' => '', 'link' => ''), $atts));
    $partner['src'][] = $src;
    $partner['link'][] = $link;
}

$atts = array(
    'name' => __('Partner', 'tfuse'),
    'desc' => __('Here comes some lorem ipsum description for the box shortcode.', 'tfuse'),
    'category' => 3,
    'options' => array(
        array(
            'name' => __('Src', 'tfuse'),
            'desc' => '',
            'id' => 'tf_shc_partner_src',
            'value' => '',
            'type' => 'text'
        ),
        array(
            'name' => __('Link', 'tfuse'),
            'desc' => '',
            'id' => 'tf_shc_partner_link',
            'value' => '',
            'type' => 'text'
        )
    )
);

add_shortcode('partner', 'tfuse_partner', $atts);