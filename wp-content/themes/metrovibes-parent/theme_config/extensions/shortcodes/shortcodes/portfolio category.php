<?php

/**
 * Testimonials
 * 
 * To override this shortcode in a child theme, copy this file to your child theme's
 * theme_config/extensions/shortcodes/shortcodes/ folder.
 * 
 * Optional arguments:
 * title:
 * order: RAND, ASC, DESC
 */
function tfuse_portfolio_cat($atts, $content = null) {
    extract(shortcode_atts(array( 'type'=>'','cat' => '','boxcol'=>'','titlecol'=>'','img'=>''), $atts));
    $output = '';
    if(!empty($cat))
    {
        $id = intval($cat);
        $desc = term_description( $id,'group' );
        $term = get_term( $id, 'group' );
        if(!empty($term))
        {
        $title = $term->name;
        $link =  get_term_link($id, 'group');
        if($type == 'type1')
        { 
            $output .= '<div class="categories-item" style="background: '.$boxcol.' !important;">
                        <div class="categories_title"><h2 ><a href="'.$link.'" style="color:'.$titlecol.'"><img src="'.$img.'" alt="" >'.$title.'</a></h2></div>
                        <div class="categories_desc clearfix" >'.$desc.'</div>
                        </div>
                        <style>
                            .categories_desc p {color:'.$titlecol.' !important;}
                        </style>';
        }
        else
        {  
             $output .= '<div class="categories-list categories-style2">
                        <div class="categories-item ">
                                <div class="categories_title"><h2  style="background: '.$boxcol.';"><a href="'.$link.'" style="color:'.$titlecol.';"><img src="'.$img.'">'.$title.'</a></h2></div>
                                <div class="categories_desc clearfix">
                                        <p>'.$desc.'</p> 
                                </div>
                                </div>
                        </div>';
        } 
        }
    }
    return $output;
}

$atts = array(
    'name' => __('Portfolio Category', 'tfuse'),
    'desc' => __('Here comes some lorem ipsum description for the box shortcode.', 'tfuse'),
    'category' => 11,
    'options' => array(
        array(
            'name' => __('Type', 'tfuse'),
            'desc' => __('Select display type', 'tfuse'),
            'id' => 'tf_shc_portfolio_type',
            'value' => 'type1',
            'options' => array(
                'type1' => 'Type 1',
                'type2' => 'Type 2'
            ),
            'type' => 'select'
        ),
        array(
            'name' => __('Category', 'tfuse'),
            'desc' => __('Type a portfolio category', 'tfuse'),
            'id' => 'tf_shc_portfolio_cat',
            'value' => '',
            'type' => 'multi',
            'subtype' => 'group',
        ),
        array(
            'name' => __('Box color', 'tfuse'),
            'desc' => __('Pick box color', 'tfuse'),
            'id' => 'tf_shc_portfolio_boxcol',
            'value' => '',
            'type' => 'colorpicker'
        ),
        array(
            'name' => __('Text color', 'tfuse'),
            'desc' => __('Pick text color', 'tfuse'),
            'id' => 'tf_shc_portfolio_titlecol',
            'value' => '',
            'type' => 'colorpicker'
        ),
        array(
            'name' => __('Icon', 'tfuse'),
            'desc' => __('URL of an image', 'tfuse'),
            'id' => 'tf_shc_portfolio_img',
            'value' => '',
            'type' => 'text'
        )
    )
);

tf_add_shortcode('portfolio', 'tfuse_portfolio_cat', $atts);
