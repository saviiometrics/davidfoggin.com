<?php

/**
 * Latest Posts
 * 
 * To override this shortcode in a child theme, copy this file to your child theme's
 * theme_config/extensions/shortcodes/shortcodes/ folder.
 * 
 * Optional arguments:
 * items:
 * title:
 * image_width:
 * image_height:
 * image_class:
 */

function tfuse_latest_posts($atts, $content = null)
{
    extract(shortcode_atts(array('items' => 5), $atts));
    $return_html = '';
    $latest_posts = tfuse_shortcode_posts(array(
                                               'sort' => 'recent',
                                               'items' => $items,
                                               'image_post' => true,
                                               'image_class' => 'thumbnail',
                                               'date_post' => true,
                                          ));
    if(!empty($latest_posts))
    {
        foreach ($latest_posts as $post_val):
        $classes = '';
        $post_date = '';
        $show_published_date = false;
        $views = get_post_meta($post_val['post_id'], TF_THEME_PREFIX . '_post_viewed', true);
        $thumbnail_image = ($src = tfuse_page_options('thumbnail_image','',$post_val['post_id'])) ?
            '<img src="'.$src.'" alt="">' : '';
        if ( tfuse_page_options('disable_published_date',tfuse_options('disable_published_date'),$post_val['post_id']) && tfuse_options('date_time'))
        {
            $show_published_date = true;
            $post_date = '<div class="post-date">
				<span class="date">'.get_the_time( 'j', $post_val['post_id'] ).'</span>
				<span class="month">'.get_the_time( 'M', $post_val['post_id'] ).'</span>
			</div>';
        }
        if (empty($src) && $show_published_date) $classes = ' tf_show_published_date2';
        $return_html .= '<div class="post-item clearfix">
			<div class="post-image">
                '.$thumbnail_image.'
            </div>
			<div class="post-title'.$classes.'"><h2><a href="'.$post_val['post_link'].'">'.$post_val['post_title'].'</a></h2></div>
			<div class="post-meta-top">
			'.$post_date.'
			</div>
			<div class="post-desc clearfix">
                            <p>'.$post_val['post_content'].'</p>
			</div>
			<div class="post-meta-bot clearfix">
				<div class="post-meta post-view"><span><i class="view-ico"></i>'.$views.'</span></div>
				<div class="post-meta post-comm"><span><i class="comment-ico"></i>'.$post_val['post_comnt_numb'].'</span></div>
				<div class="post-meta post-author"><span><i class="pencil-ico"></i>'.$post_val['post_author_name'].'</span></div>
				<div class="post-meta post-read"><a href="'. $post_val['post_link'].'" class="text-center "><i class="read-ico"></i><span>'.__("Read More","tfuse").'</span></a></div>
			</div>
			</div>';
       
    endforeach;
    }
    return $return_html;
}

$atts = array(
    'name' => __('Latest Posts', 'tfuse'),
    'desc' => __('Here comes some lorem ipsum description for the box shortcode.', 'tfuse'),
    'category' => 11,
    'options' => array(
        array(
            'name' => __('Items', 'tfuse'),
            'desc' => __('Specifies the number of the post to show', 'tfuse'),
            'id' => 'tf_shc_latest_posts_items',
            'value' => '5',
            'type' => 'text'
        )
    )
);

tf_add_shortcode('latest_posts', 'tfuse_latest_posts', $atts);
