<?php

function tfuse_members($atts, $content = null) {
    extract(shortcode_atts(array( 'order' => ''), $atts));

    $output = '';  

    if (!empty($order) && ($order == 'ASC' || $order == 'DESC'))
        $order = '&order=' . $order;
    else
        $order = '&orderby=rand';

    $posts = get_posts('post_type=members&posts_per_page=-1' . $order);
    $k = 0;
    $sidebar_position = tfuse_sidebar_position();
    $output .='<div class="team_list clearfix">';
    foreach ($posts as $post ) {
        $k++;
        $img = tfuse_page_options('img','',$post->ID);
        $twitter = tfuse_page_options('member_twitter','',$post->ID);
        $facebook = tfuse_page_options('member_facebook','',$post->ID);
        $pinterest = tfuse_page_options('member_pinterest','',$post->ID);

        if(!empty($img)) $img = '<img src="'.$img.'" width="112" height="112" alt="">';
        if(($sidebar_position == 'full'))
        {
            $output .= '
                    <div class="team-box">
                        <div class="team-description clearfix">
                		<div class="team-image">'.$img.'</div>
                      	<div class="team-text">
                        	<h4>'.$post->post_title.'</h4>
                            <p>'.strip_tags(apply_filters('the_content',$post->post_content)).'</p>
                        </div>
                        </div>
                        <div class="team-contact">';
			if(!empty($twitter)) $output .= ' <a href="'.$twitter.'" target="_blank" class="team_link_1"><span>'.__('Twitter','tfuse').'</span></a>';
            if(!empty($facebook)) $output .= ' <a href="'.$facebook.'" target="_blank" class="team_link_2"><span>'.__('Facebook','tfuse').'</span></a>';
            if(!empty($pinterest)) $output .= ' <a href="'.$pinterest.'" target="_blank" class="team_link_3"><span>'.__('Pinterest','tfuse').'</span></a>';
              $output .= '</div>
                        <div class="clear"></div>
                        </div>';
              if($k % 3 == 0 )  $output .= '<div class="clear"></div>';
        }
        else
        {
             $output .= '
                    <div class="team-box" style="width:290px;">
                        <div class="team-description clearfix">
                		<div class="team-image">'.$img.'</div>
                      	<div class="team-text">
                        	<h4>'.$post->post_title.'</h4>
                            <p>'.strip_tags(apply_filters('the_content',$post->post_content)).'</p>
                        </div>
                        </div>
                        <div class="team-contact">';
			if(!empty($twitter)) $output .= ' <a href="'.$twitter.'" target="_blank" class="team_link_1"><span>'.__('Twitter','tfuse').'</span></a>';
            if(!empty($facebook)) $output .= ' <a href="'.$facebook.'" target="_blank" class="team_link_2"><span>'.__('Facebook','tfuse').'</span></a>';
            if(!empty($pinterest)) $output .= ' <a href="'.$pinterest.'" target="_blank" class="team_link_3"><span>'.__('Pinterest','tfuse').'</span></a>';
              $output .= '</div>
                        <div class="clear"></div>
                        </div>';
              if($k % 2 == 0 )  $output .= '<div class="clear"></div>';
        }
        

    } // End WHILE Loop
    $output .= '<div class="clear"></div>';
    
    return $output;
}

$atts = array(
    'name' => __('Members', 'tfuse'),
    'desc' => __('Here comes some lorem ipsum description for the box shortcode.', 'tfuse'),
    'category' => 20,
    'options' => array(
        array(
            'name' => __('Order', 'tfuse'),
            'desc' => __('Select display order', 'tfuse'),
            'id' => 'tf_shc_members_order',
            'value' => 'ASC',
            'options' => array(
                'RAND' => __('Random', 'tfuse'),
                'ASC' => __('Ascending', 'tfuse'),
                'DESC' => __('Descending', 'tfuse')
            ),
            'type' => 'select'
        )
    )
);

tf_add_shortcode('members', 'tfuse_members', $atts);
