<?php
function tfuse_social($atts, $content = null)
{
    extract(shortcode_atts(array('facebook' => '',  'pinterest' => '', 'twitter' => ''), $atts));
    $out ='';
    
    $out .= '<div class="footer_social ">';
    if(!empty($facebook)) $out .= '<a href="'.$facebook.'" target="_blank" class="social-fb"><span>'.__('Facebook','tfuse').'</span></a>';
    if(!empty($pinterest)) $out .= '<a href="'.$pinterest.'" target="_blank" class="social-pinterest"><span>'.__('Pinterest','tfuse').'</span></a>';
    if(!empty($twitter)) $out .= '<a href="'.$twitter.'" target="_blank" class="social-twitter"><span>'.__('Twitter','tfuse').'</span></a>';
    $out .='</div>';
    
    return $out;
}

$atts = array(
    'name' => __('Social', 'tfuse'),
    'desc' => __('Here comes some lorem ipsum description for the social shortcode.', 'tfuse'),
    'category' => 9,
    'options' => array(
        array(
            'name' => __('Facebook', 'tfuse'),
            'desc' => __('Facebook link', 'tfuse'),
            'id' => 'tf_shc_social_facebook',
            'value' => '',
            'type' => 'text'
        ),
        array(
            'name' => __('Pinterest', 'tfuse'),
            'desc' => __('Pinterest link', 'tfuse'),
            'id' => 'tf_shc_social_pinterest',
            'value' => '',
            'type' => 'text'
        ),
        array(
            'name' => __('Twitter', 'tfuse'),
            'desc' => __('Twitter link', 'tfuse'),
            'id' => 'tf_shc_social_twitter',
            'value' => '',
            'type' => 'text'
        )
    )
);

tf_add_shortcode('social', 'tfuse_social', $atts);
