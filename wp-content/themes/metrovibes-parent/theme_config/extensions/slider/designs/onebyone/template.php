<?php
global $TFUSE;
$TFUSE->include->register_type('slider_css_folder', get_template_directory() . '/theme_config/extensions/slider/designs/'.$slider['design'].'/static/css');
$TFUSE->include->css('jquery.onebyone', 'slider_css_folder', 'tf_head');

$TFUSE->include->register_type('slider_js_folder', get_template_directory() . '/theme_config/extensions/slider/designs/'.$slider['design'].'/static/js');
$TFUSE->include->js('jquery.onebyone.min', 'slider_js_folder', 'tf_head');

$style = '';
if($slider['general']['slider_image_bg'] == 'custom')
    $style .= 'style="background-image:url('.$slider['general']['slider_img_bg'].');background-color:'.$slider['general']['slider_color_bg'].';"';
else
    $style = ''.tfuse_change_header_bg().'';
?>
<!-- header -->
<div class="header header-medium" <?php echo $style; ?> >
    <div class="header_inner">
        <div class="container">
            <!-- header slider -->
            <div class="header_slider">
                <?php foreach ($slider['slides'] as $slide):?>
                    <div class="oneByOne_item">
                        <div class="slider_title" data-animate="flipInY">
                        <a href="<?php echo $slide['slide_link'];?>"><strong><?php echo $slide['slide_title'];?></strong></a>
                    </div>	                    
                        <img src="<?php echo $slide['slide_src'];?>" class="slider_image" data-animate="fadeInLeft" alt="">
                        <a href="<?php echo $slide['slide_link'];?>" class="slider_btn" data-animate="fadeInDown"><span><?php _e('Find out more','tfuse'); ?></span></a>
                    </div>
                <?php endforeach; ?>
            </div>
            <script>	
                jQuery(document).ready(function($) {		
                 jQuery('.header_slider').oneByOne({
                        className: 'oneByOne1',	 
                        slideShow: true,
                        width: 960,
                        height: 384,
                        slideShowDelay: 4000,
                        showButton: false,
                        autoHideButton: false,
                        enableDrag: false,
                        tolerance: 0
                    });		
                });
            </script> 
            <!--/ header slider -->
        </div>
    </div>
</div>
<!--/ header -->