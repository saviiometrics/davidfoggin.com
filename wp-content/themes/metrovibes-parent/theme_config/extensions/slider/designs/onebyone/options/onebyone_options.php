<?php
/**
 * Play slider's configurations
 *
 * @since  MetroVibes 1.0
 */

$options = array(
    'tabs' => array(
        array(
            'name' => __('Slider Settings', 'tfuse'),
            'id' => 'slider_settings', #do no t change this ID
            'headings' => array(
                array(
                    'name' => __('Slider Settings', 'tfuse'),
                    'options' => array(
                        array('name' => __('Slider Title', 'tfuse'),
                            'desc' => __('Change the title of your slider', 'tfuse'),
                            'id' => 'slider_title',
                            'value' => '',
                            'type' => 'text',
                            'divider' => true),
                        array('name' => __('Background', 'tfuse'),
                            'desc' => __('Slider background', 'tfuse') ,
                            'id' => 'slider_image_bg',
                            'value' => 'framework',
                            'options' => array('custom'=>__('Use Slider custom background', 'tfuse'),'framework'=>__('Use default background', 'tfuse')),
                            'type' => 'select'),
                        array('name' => __('Image', 'tfuse'),
                            'desc' => __('Upload background image', 'tfuse'),
                            'id' => 'slider_img_bg',
                            'value' => '',
                            'type' => 'upload'),
                        array('name' => __('Color', 'tfuse'),
                            'desc' => __('Choose background color', 'tfuse'),
                            'id' => 'slider_color_bg',
                            'value' => '',
                            'type' => 'colorpicker',
                            'divider' => true),
			array('name' => __('Resize images?', 'tfuse'),
                            'desc' => __('Want to let our script to resize the images for you? Or do you want to have total control and upload images with the exact slider image size?', 'tfuse'),
                            'id' => 'slider_image_resize',
                            'value' => 'false',
                            'type' => 'checkbox')
                    )
                )
            )
        ),
        array(
            'name' => __('Add/Edit Slides', 'tfuse'),
            'id' => 'slider_setup', #do not change ID
            'headings' => array(
                array(
                    'name' => __('Add New Slide', 'tfuse'), #do not change
                    'options' => array(
                       
                        array('name' => __('Title', 'tfuse'),
                            'desc' => ' The Title is displayed on the image and will be visible by the users',
                            'id' => 'slide_title',
                            'value' => '',
                            'type' => 'text',
                            'required' => false,
                            'divider' => true),
                         array('name' => __('URL', 'tfuse'),
                            'desc' => __('When a user will click the image, the browser will load this URL.', 'tfuse'),
                            'id' => 'slide_link',
                            'value' => '',
                            'type' => 'text',
                            'required' => false,
                            'divider' => true),
                        array('name' => __('Image <br />', 'tfuse'),
                            'desc' => __('You can upload an image from your hard drive or use one that was already uploaded by pressing  "Insert into Post" button from the image uploader plugin.', 'tfuse'),
                            'id' => 'slide_src',
                            'value' => '',
                            'type' => 'upload',
                            'media' => 'image',
                            'required' => TRUE)
                    )
                )
            )
        )
    )
);

$options['extra_options'] = array();
?>