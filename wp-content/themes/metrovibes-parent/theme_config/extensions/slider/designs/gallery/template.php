<div class="header" <?php echo tfuse_change_header_bg();?>>
    <div class="header_inner">
        <div class="container">
            <div class="grid_gallery clearfix">
                <?php $count = 0;  foreach ($slider['slides'] as $slide): $count++;?>
                    <?php if($count == 8) break; if($count == 1){ ?>
                        <div class="box box-1">
                            <a href="<?php echo $slide['slide_url']?>" ><img src="<?php echo $slide['slide_src']?>" width="306" height="391" alt="" /><span><?php _e('more','tfuse');?></span></a>
                        </div>
                    <?php } else {?>
                        <div class="box box-2">
                            <a href="<?php echo $slide['slide_url']?>" ><img src="<?php echo $slide['slide_src']?>" alt="" /><span><?php _e('more','tfuse');?></span></a>
                        </div>
                    <?php }?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>