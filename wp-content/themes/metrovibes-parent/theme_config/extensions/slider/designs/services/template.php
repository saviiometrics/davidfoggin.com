<!-- header -->
<div class="header header_big"  <?php echo tfuse_change_header_bg();?>>
    <div class="header_inner">
        <div class="container">
            <div class="header-title">
                <h1><?php echo $slider['general']['slider_title'] ;?></h1>
                <h5><?php echo $slider['general']['slider_subtitle'] ;?></h5>
            </div>
            <!-- carousel with services -->
            <div class="minigallery_carousel">		            
                <div class="carousel_content">
                    <ul id="minigallery2">
                        <?php foreach ($slider['slides'] as $slide):?>
                            <li>
                                <div class="service_item">
                                    <div class="service_img"><img src="<?php echo $slide['img_src']?>" width="112" height="112" alt=""></div>
                                    <div class="service_title"><h2><?php echo $slide['title']?></h2></div>
                                    <div class="service_desc">
                                        <p><?php echo $slide['description']?></p>
                                    </div>
                                    <div class="service_meta_bot"><a href="<?php echo $slide['slide_url']?>" class="link-more"><?php _e('Related works','tfuse');?></a></div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>   
            <a class="prev" id="minigallery2_prev" href="#"><span><?php _e('prev','tfuse');?></span></a>
            <a class="next" id="minigallery2_next" href="#"><span><?php _e('next','tfuse');?></span></a>
            </div>
            <!-- /carousel -->
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($) {
        jQuery('#minigallery2').carouFredSel({
            next : "#minigallery2_next",
            prev : "#minigallery2_prev",
            pagination: "#minigallery2_pag",
            auto: false,
            circular: true,
            infinite: true,	
            width: '100%',
            height: "auto",		
            scroll: {
                    items : 1					
            }		
        });
    });
</script>
<!--/ header -->
<!--header bottom-->
<div class="header-bottom">
        <div class="minigallery_pagination">
        <div class="pagination" id="minigallery2_pag"></div>
        </div>
</div>