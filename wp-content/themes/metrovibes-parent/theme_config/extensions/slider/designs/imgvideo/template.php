<!-- header -->
<div class="header" <?php echo tfuse_change_header_bg();?>>
    <div class="header_inner">
        <div class="container">
            <!-- header slider -->
            <div class="header_slider vidimg_slider">
                <div id="top_slider2">
                    <?php foreach ($slider['slides'] as $slide):?>
                        <?php 
                            if($slide['slide_align_img'] == 'alignright') $pos = 'slide_image_right';
                            else $pos = '';
                        ?>
                        <?php if(!empty($slide['slide_media'])){?>
                            <?php 
							preg_match('|<iframe [^>]*(src="[^"]+")[^>]*|', $slide['slide_media'], $iframe);
							if(!empty($iframe))
							{
								$video = $slide['slide_media'];
							}
							else
							{
                                preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $slide['slide_media'], $video_id);
                                if(!empty($video_id)) $video .= '<iframe src="http://www.youtube.com/embed/'.$video_id[0].'?wmode=transparent"  width="426" height="240" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
                                else $video = '<iframe src="'.$slide['slide_media'].'"  frameborder="0" width="426" height="240" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
                           
							}
							?>
                    <div class="slider_item <?php echo $pos;?>">
                                <div class="slider_image">
                                    <?php echo $video;?>
                                </div>
                                    <div class="slider_text">
                                    <h2><a href="<?php echo $slide['slide_url'];?>"><?php echo $slide['slide_title'];?></a></h2>
                                    <p><?php echo $slide['slide_description'];?></p>
                                </div>	                    	                    
                            </div>
                        <?php }else {?>
                            <div class="slider_item <?php echo $pos;?>">
                                <div class="slider_image">
                                    <a href="<?php echo $slide['slide_url'];?>"><img src="<?php echo $slide['slide_src'];?>" alt=""></a>
                                </div>
                                    <div class="slider_text">
                                        <h2><a href="<?php echo $slide['slide_url'];?>"><?php echo $slide['slide_title'];?></a></h2>
                                        <p><?php echo $slide['slide_description'];?></p>
                                    </div>	                    	                    
                            </div>
                        <?php }?>
                    <?php endforeach; ?>
                </div>
                <a class="prev" id="top_slider2_prev" href="#"><span><?php _e('prev','tfuse');?></span></a>	                
                <a class="next" id="top_slider2_next" href="#"><span><?php _e('next','tfuse');?></span></a>
            </div>
            <script>	
                jQuery(document).ready(function($) {		
                jQuery('#top_slider2').carouFredSel({
                        next : "#top_slider2_next",
                        prev : "#top_slider2_prev",
                        infinite: false,
                        auto: {
                                play: true,
                                timeoutDuration: 5000
                        },
                        width: '100%',		
                        scroll: {
                                items : 1,
                                fx: "fade",
                                easing: "linear",
                                pauseOnHover: true
                        }
                    });
                });
            </script> 
            <!--/ header slider -->
        </div>
    </div>
</div>
<!--/ header -->