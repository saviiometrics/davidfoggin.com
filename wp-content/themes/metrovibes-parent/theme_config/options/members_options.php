<?php

/* ----------------------------------------------------------------------------------- */
/* Initializes all the theme settings option fields for posts area. */
/* ----------------------------------------------------------------------------------- */

$options = array(
    /* ----------------------------------------------------------------------------------- */
    /* Sidebar */
    /* ----------------------------------------------------------------------------------- */


    /* ----------------------------------------------------------------------------------- */
    /* After Textarea */
    /* ----------------------------------------------------------------------------------- */

    /* Post Media */
    array('name' => __('Member Info', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_media',
        'type' => 'metabox',
        'context' => 'normal'
    ),
    // Subtitle
    array('name' => __('Avatar', 'tfuse'),
        'desc' => __('Upload member\'s avatar. ', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_img',
        'value' => '',
        'type' => 'upload'
    ),
   
    
    /* Content Options */
    array('name' => __('Social', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_content_social',
        'type' => 'metabox',
        'context' => 'normal'
    ),
    // Top Shortcodes
    array('name' => __('Twitter', 'tfuse'),
        'desc' => __('Twitter link.', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_member_twitter',
        'value' => '',
        'type' => 'text'
    ),
    array('name' => __('Facebook', 'tfuse'),
        'desc' => __('Facebook link.', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_member_facebook',
        'value' => '',
        'type' => 'text'
    ),
    // Top Shortcodes
    array('name' => __('Pinterest', 'tfuse'),
        'desc' => __('Pinterest link.', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_member_pinterest',
        'value' => '',
        'type' => 'text'
    )
    
);

?>