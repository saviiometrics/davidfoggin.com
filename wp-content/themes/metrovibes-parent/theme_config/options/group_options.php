<?php

/* ----------------------------------------------------------------------------------- */
/* Initializes all the theme settings option fields for categories area.             */
/* ----------------------------------------------------------------------------------- */

$options = array(

      //Posts Display
    array('name' => __('Display Type', 'tfuse'),
        'desc' => __('Select your preferred display type', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_display_type',
        'value' => tfuse_options('display_type'),
        'options' => array(
            '3col' => array(get_template_directory_uri() . '/images/3column.png', '3 columns type'),
            '4col' => array(get_template_directory_uri() . '/images/4column.png', '4 columns type'),
            ),
        'type' => 'images',
        'divider' => true
    ),
  // Element of Hedear
    array('name' => __('Element of Hedear', 'tfuse'),
        'desc' => __('Select type of element on the header.', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_header_element',
        'value' => 'without',
        'options' => array('without' => __('Without Header Element', 'tfuse'),'slider' => __('Slider on Header', 'tfuse'),'title' => __('Title on Header', 'tfuse'),'video' => 'Video on Header','map' => __('Map on Header', 'tfuse')),
        'type' => 'select',
    ),
    // Select Header Slider
    $this->ext->slider->model->has_sliders() ?
            array(
        'name' => __('Slider', 'tfuse'),
        'desc' => __('Select a slider for your post. The sliders are created on the', 'tfuse') . '<a href="' . admin_url( 'admin.php?page=tf_slider_list' ) . '" target="_blank">' . __('Sliders page', 'tfuse') . '</a>.',
        'id' => TF_THEME_PREFIX . '_select_slider',
        'value' => '',
        'options' => $TFUSE->ext->slider->get_sliders_dropdown(),
        'type' => 'select'
            ) :
            array(
        'name' => __('Slider', 'tfuse'),
        'desc' => '',
        'id' => TF_THEME_PREFIX . '_select_slider',
        'value' => '',
        'html' => __('No sliders created yet. You can start creating one', 'tfuse') . '<a href="' . admin_url('admin.php?page=tf_slider_list') . '">' . __('here', 'tfuse') . '</a>.',
        'type' => 'raw'
            )
    ,
    //map on header
   array(
            'name'=>__('Adress', 'tfuse'),
            'desc'=>__('Enter your adress.', 'tfuse'),
            'id'=> TF_THEME_PREFIX . '_header_adress',
            'value' => '',
            'type' =>'text'
    ),
    array(
            'name'=>__('Email', 'tfuse'),
            'desc'=>__('Enter the email.', 'tfuse'),
            'id'=> TF_THEME_PREFIX . '_header_email',
            'value' => '',
            'type' =>'text'
    ),
    array(
            'name'=>__('Phone', 'tfuse'),
            'desc'=>__('Enter the phone number.', 'tfuse'),
            'id'=> TF_THEME_PREFIX . '_header_phone',
            'value' => '',
            'type' =>'text'
    ),
    
    array(
        'name' => __('Map position', 'tfuse'),
        'desc' => '',
        'id' => TF_THEME_PREFIX . '_page_map',
        'value' => '',
        'type' => 'maps'
    ),
    
    //title/subtitle and image
     array(
            'name'=>__('Title', 'tfuse'),
            'desc'=>__('Enter the title.', 'tfuse'),
            'id'=> TF_THEME_PREFIX . '_header_title',
            'value' => '',
            'type' =>'text'
    ),
    array(
            'name'=>__('Subtitle', 'tfuse'),
            'desc'=>__('Enter subtitle.', 'tfuse'),
            'id'=> TF_THEME_PREFIX . '_header_subtitle',
            'value' => '',
            'type' =>'text'
    ),
     array(
            'name'=>__('Video URL', 'tfuse'),
            'desc'=>__('Enter the url.', 'tfuse'),
            'id'=> TF_THEME_PREFIX . '_header_video',
            'value' => '',
            'type' =>'text'
    ),
    // Bottom Shortcodes
    array('name' => __('Shortcodes after Content', 'tfuse'),
        'desc' => __('In this textarea you can input your prefered custom shotcodes.', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_content_bottom',
        'value' => '',
        'type' => 'textarea'
    )
   
);

?>