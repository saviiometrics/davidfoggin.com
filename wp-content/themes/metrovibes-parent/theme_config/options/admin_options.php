<?php

/* ----------------------------------------------------------------------------------- */
/* Initializes all the theme settings option fields for admin area. */
/* ----------------------------------------------------------------------------------- */

$options = array(
    'tabs' => array(
        array(
            'name' => __('General', 'tfuse'),
            'type' => 'tab',
            'id' => TF_THEME_PREFIX . '_general',
            'headings' => array(
                array(
                    'name' => __('General Settings', 'tfuse'),
                    'options' => array(/* 1 */
                        array(
                            'name' => __('Logo type', 'tfuse'),
                            'desc' => __('Choose your preferred.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_choose_logo',
                            'value' => 'text',
                            'options' =>  array('custom' => __('Custom Logo', 'tfuse'), 'text' => __('Text Logo', 'tfuse')),
                            'type' => 'select'
                        ),
                        // Custom Logo Option
                        array(
                            'name' => __('Custom Logo', 'tfuse'),
                            'desc' => __('Upload a logo for your theme, or specify the image address of your online logo. (http://yoursite.com/logo.png)', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_logo',
                            'value' => '',
                            'type' => 'upload'
                        ),
                         // Custom Logo Text Option
                        array(
                            'name' => __('Text Logo', 'tfuse'),
                            'desc' => __('Enter your text for logo', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_logo_text',
                            'value' => 'Gadgetry',
                            'type' => 'text',
                             'divider' => true
                        ),
                        
                        // Custom Favicon Option
                        array(
                            'name' => __('Custom Favicon <br /> (16px x 16px)', 'tfuse'),
                            'desc' => __('Upload a 16px x 16px Png/Gif image that will represent your website\'s favicon.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_favicon',
                            'value' => '',
                            'type' => 'upload',
                            'divider' => true
                        ),
                        // Change default avatar
                        array(
                            'name' => __('Default Avatar','tfuse'),
                            'desc' => __('For users without a custom avatar of their own, you can either display a generic logo or a generated one based on their e-mail address.','tfuse'),
                            'id' => TF_THEME_PREFIX . '_default_avatar',
                            'value' => '',
                            'type' => 'upload',
                            'divider' => true
                        ),
                        //choose pagination
                         array('name' => __('Pagination Type', 'tfuse'),
                            'desc' => __('Choose pagination type.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_pagination_type',
                            'value' => 'type1',
                            'options' => array('type1' => 'Number pagination', 'type2' => 'Read more pagination'),
                            'type' => 'select',
                            'divider' => true
                        ),
                        //date
                        array(
                            'name' => __('Enable Date', 'tfuse'),
                            'desc' => __('Enable date across the site.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_date_time',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        
                        // Tracking Code Option
                        array(
                            'name' => __('Tracking Code', 'tfuse'),
                            'desc' => __('Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_google_analytics',
                            'value' => '',
                            'type' => 'textarea',
                            'divider' => true
                        ),
                        // Custom CSS Option
                        array(
                            'name' => __('Custom CSS', 'tfuse'),
                            'desc' => __('Quickly add some CSS to your theme by adding it to this block.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_custom_css',
                            'value' => '',
                            'type' => 'textarea'
                        )
                    ) /* E1 */
                ),
				array(
                    'name' => __('Categories', 'tfuse'),
                    'options' => array(
                        // Element of Hedear
                        array('name' => __('Listing All Blog Posts Header Element', 'tfuse'),
                            'desc' => __('Select type of element on the header for listing blog posts.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_header_element_posts',
                            'value' => 'without',
                            'options' => array('without' => __('Without Header Element', 'tfuse'),'slider' => __('Slider on Header', 'tfuse'),'title' => __('Title on Header', 'tfuse'),'video' => 'Video on Header','map' => __('Map on Header', 'tfuse')),
                            'type' => 'select',
                        ),
                        // Select Header Slider
                        $this->ext->slider->model->has_sliders() ?
                                array(
                            'name' => __('Slider', 'tfuse'),
                            'desc' => __('Select a slider for your post. The sliders are created on the', 'tfuse') . '<a href="' . admin_url( 'admin.php?page=tf_slider_list' ) . '" target="_blank">' . __('Sliders page', 'tfuse') . '</a>.',
                            'id' => TF_THEME_PREFIX . '_select_slider_posts',
                            'value' => '',
                            'options' => $TFUSE->ext->slider->get_sliders_dropdown(),
                            'type' => 'select'
                                ) :
                                array(
                            'name' => __('Slider', 'tfuse'),
                            'desc' => '',
                            'id' => TF_THEME_PREFIX . '_select_slider_posts',
                            'value' => '',
                            'html' => __('No sliders created yet. You can start creating one', 'tfuse') . '<a href="' . admin_url('admin.php?page=tf_slider_list') . '">' . __('here', 'tfuse') . '</a>.',
                            'type' => 'raw'
                                )
                        ,
                        //map on header
                       array(
                                'name'=>__('Adress', 'tfuse'),
                                'desc'=>__('Enter your adress.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_adress_posts',
                                'value' => '',
                                'type' =>'text'
                        ),
                        array(
                                'name'=>__('Email', 'tfuse'),
                                'desc'=>__('Enter the email.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_email_posts',
                                'value' => '',
                                'type' =>'text'
                        ),
                        array(
                                'name'=>__('Phone', 'tfuse'),
                                'desc'=>__('Enter the phone number.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_phone_posts',
                                'value' => '',
                                'type' =>'text'
                        ),

                        array(
                            'name' => __('Map position', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_page_map_posts',
                            'value' => '',
                            'type' => 'maps'
                        ),

                        //title/subtitle and image
                         array(
                                'name'=>__('Title', 'tfuse'),
                                'desc'=>__('Enter the title.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_title_posts',
                                'value' => '',
                                'type' =>'text'
                        ),
                        array(
                                'name'=>__('Subtitle', 'tfuse'),
                                'desc'=>__('Enter subtitle.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_subtitle_posts',
                                'value' => '',
                                'type' =>'text'
                        ),
                         array(
                                'name'=>__('Video URL', 'tfuse'),
                                'desc'=>__('Enter the url.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_video_posts',
                                'value' => '',
                                'type' =>'text',
                                'divider' => true
                        ),
                        // Bottom Shortcodes
                        array('name' => __('Listing Blog Posts Shortcodes after Content', 'tfuse'),
                            'desc' => __('In this textarea you can input your prefered custom shotcodes.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_content_bottom_posts',
                            'value' => '',
                            'type' => 'textarea'
                        )
                    )
                ),
                array(
                    'name' => __('Portfolio', 'tfuse'),
                    'options' => array(
                         //Posts Display
                        array('name' => __('Default Display Type', 'tfuse'),
                            'desc' => __('Select your preferred default display type for portfolio.When you create a new portfolio,this will be the default display type for that portfolio.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_display_type',
                            'value' => '3col',
                            'options' => array(
                                '3col' => array(get_template_directory_uri() . '/images/3column.png', '3 columns type'),
                                '4col' => array(get_template_directory_uri() . '/images/4column.png', '4 columns type'),
                                ),
                            'type' => 'images',
                            'divider' => true
                        ),
                        array('name' => __('Portfolio Type', 'tfuse'),
                            'desc' => __('Select your preferred  display type for portfolio posts.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_portf_type',
                            'value' => 'pretty',
                            'options' => array(
                                'pretty' => __('Gallery', 'tfuse'),
                                'single' => __('With Single', 'tfuse'),
                                ),
                            'type' => 'select',
                            'divider' => true
                        ),
                        // Element of Hedear
                        array('name' => __('Listing All Portfolio Items Header Element', 'tfuse'),
                            'desc' => __('Select type of element on the header for listing portfolio items.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_header_element_portf',
                            'value' => 'without',
                            'options' => array('without' => __('Without Header Element', 'tfuse'),'slider' => __('Slider on Header', 'tfuse'),'title' => __('Title on Header', 'tfuse'),'video' => 'Video on Header','map' => __('Map on Header', 'tfuse')),
                            'type' => 'select',
                        ),
                        // Select Header Slider
                        $this->ext->slider->model->has_sliders() ?
                                array(
                            'name' => __('Slider', 'tfuse'),
                            'desc' => __('Select a slider for your post. The sliders are created on the', 'tfuse') . '<a href="' . admin_url( 'admin.php?page=tf_slider_list' ) . '" target="_blank">' . __('Sliders page', 'tfuse') . '</a>.',
                            'id' => TF_THEME_PREFIX . '_select_slider_portf',
                            'value' => '',
                            'options' => $TFUSE->ext->slider->get_sliders_dropdown(),
                            'type' => 'select'
                                ) :
                                array(
                            'name' => __('Slider', 'tfuse'),
                            'desc' => '',
                            'id' => TF_THEME_PREFIX . '_select_slider_portf',
                            'value' => '',
                            'html' => __('No sliders created yet. You can start creating one', 'tfuse') . '<a href="' . admin_url('admin.php?page=tf_slider_list') . '">' . __('here', 'tfuse') . '</a>.',
                            'type' => 'raw'
                                )
                        ,
                        //map on header
                       array(
                                'name'=>__('Adress', 'tfuse'),
                                'desc'=>__('Enter your adress.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_adress_portf',
                                'value' => '',
                                'type' =>'text'
                        ),
                        array(
                                'name'=>__('Email', 'tfuse'),
                                'desc'=>__('Enter the email.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_email_portf',
                                'value' => '',
                                'type' =>'text'
                        ),
                        array(
                                'name'=>__('Phone', 'tfuse'),
                                'desc'=>__('Enter the phone number.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_phone_portf',
                                'value' => '',
                                'type' =>'text'
                        ),

                        array(
                            'name' => __('Map position', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_page_map_portf',
                            'value' => '',
                            'type' => 'maps'
                        ),

                        //title/subtitle and image
                         array(
                                'name'=>__('Title', 'tfuse'),
                                'desc'=>__('Enter the title.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_title_portf',
                                'value' => '',
                                'type' =>'text'
                        ),
                        array(
                                'name'=>__('Subtitle', 'tfuse'),
                                'desc'=>__('Enter subtitle.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_subtitle_portf',
                                'value' => '',
                                'type' =>'text'
                        ),
                         array(
                                'name'=>__('Video URL', 'tfuse'),
                                'desc'=>__('Enter the url.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_video_portf',
                                'value' => '',
                                'type' =>'text',
                                'divider' => true
                        ),
                        // Bottom Shortcodes
                        array('name' => __('Listing All Portfolio Items Shortcodes after Content', 'tfuse'),
                            'desc' => __('In this textarea you can input your prefered custom shotcodes.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_content_bottom_portf',
                            'value' => '',
                            'type' => 'textarea'
                        )
                    )
                ),
                array(
                    'name' => __('Services', 'tfuse'),
                    'options' => array(
                        array('name' => __('Use', 'tfuse'),
                            'desc' => __('Select one of the options.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_service_link',
                            'value' => 'link',
                            'options' => array(
                                'link' => __('Custom Link', 'tfuse'),
                                'single' => __('Single Page', 'tfuse'),
                                ),
                            'type' => 'select'
                        )
                    )
                ),
                array(
                    'name' => __('Twitter', 'tfuse'),
                    'options' => array(
                        array(
                            'name' => __('Consumer Key', 'tfuse'),
                            'desc' => __('Set your <a href="http://screencast.com/t/zHu17C7nXy1">twitter</a> application <a href="http://screencast.com/t/yb44HiF2NZ">consumer key</a>.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_twitter_consumer_key',
                            'value' => 'XW7t8bECoR6ogYtUDNdjiQ',
                            'type' => 'text',
                            'divider' => true
                        ),
                        array(
                            'name' => __('Consumer Secret', 'tfuse'),
                            'desc' => __('Set your <a href="http://screencast.com/t/zHu17C7nXy1">twitter</a> application <a href="http://screencast.com/t/eaKJHG1omN">consumer secret key</a>.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_twitter_consumer_secret',
                            'value' => 'Z7UzuWU8a4obyOOlIguuI4a5JV4ryTIPKZ3POIAcJ9M',
                            'type' => 'text',
                            'divider' => true
                        ),
                        array(
                            'name' => __('User Token', 'tfuse'),
                            'desc' => __('Set your <a href="http://screencast.com/t/zHu17C7nXy1">twitter</a> application <a href="http://screencast.com/t/QEEG2O4H">access token key</a>.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_twitter_user_token',
                            'value' => '1510587853-ugw6uUuNdNMdGGDn7DR4ZY4IcarhstIbq8wdDud',
                            'type' => 'text',
                            'divider' => true
                        ),
                        array(
                            'name' => __('User Secret', 'tfuse'),
                            'desc' => __('Set your <a href="http://screencast.com/t/zHu17C7nXy1">twitter</a>  application <a href="http://screencast.com/t/Yv7nwRGsz">access token secret key</a>.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_twitter_user_secret',
                            'value' => '7aNcpOUGtdKKeT1L72i3tfdHJWeKsBVODv26l9C0Cc',
                            'type' => 'text'
                        )
                    )
                ),
                array(
                    'name' => __('RSS', 'tfuse'),
                    'options' => array(
                        // RSS URL Option
                        array('name' => __('RSS URL', 'tfuse'),
                            'desc' => __('Enter your preferred RSS URL. (Feedburner or other)', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_feedburner_url',
                            'value' => '',
                            'type' => 'text',
                            'divider' => true
                        ),
                        // E-Mail URL Option
                        array('name' => __('E-Mail URL', 'tfuse'),
                            'desc' => __('Enter your preferred E-mail subscription URL. (Feedburner or other)', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_feedburner_id',
                            'value' => '',
                            'type' => 'text'
                        )
                    )
                ),
                array(
                    'name' => __('Enable Theme settings', 'tfuse'),
                    'options' => array(
                        // Disable Image for All Single Posts
                        array('name' => __('Image on Single Post', 'tfuse'),
                            'desc' => __('Enable Image on All Single Posts? These settings may be overridden for individual articles.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_image',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        // Disable Video for All Single Posts
                        array('name' => __('Video on Single Post', 'tfuse'),
                            'desc' => __('Enable Video on All Single Posts? These settings may be overridden for individual articles.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_video',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        // Disable Comments for All Posts
                        array('name' => __('Post Comments', 'tfuse'),
                            'desc' => __('Enable Comments for All Posts? These settings may be overridden for individual articles.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_posts_comments',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        
                        // Disable Author Info
                        array('name' => __('Author Info', 'tfuse'),
                            'desc' => __('Enable Author Info? These settings may be overridden for individual articles.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_author_info',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        // Disable Post Meta
                        array('name' => __('Post meta', 'tfuse'),
                            'desc' => __('Enable Post meta? These settings may be overridden for individual articles.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_post_meta',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        // Disable Post Published Date
                        array('name' => __('Post Published Date', 'tfuse'),
                            'desc' => __('Enable Post Published Date? These settings may be overridden for individual articles.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_published_date',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        // Disable posts lightbox (prettyPhoto) Option
                        array('name' => 'prettyPhoto on Categories',
                            'desc' => __('Enable opening image and attachemnts in prettyPhoto on Categories listings? If YES, image link go to post.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_listing_lightbox',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        // Disable posts lightbox (prettyPhoto) Option
                        array('name' => 'prettyPhoto on Single Post',
                            'desc' => __('Enable opening image and attachemnts in prettyPhoto on Single Post?', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_single_lightbox',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        // Disable preloadCssImages plugin
                        array('name' => 'preloadCssImages',
                            'desc' => __('Enable jQuery-Plugin "preloadCssImages"? This plugin loads automatic all images from css.If you prefer performance(less requests) deactivate this plugin.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_preload_css',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'on_update' => 'reload_page',
                            'divider' => true
                        ),
                        // Disable SEO
                        array('name' => __('SEO Tab', 'tfuse'),
                            'desc' => __('Enable SEO option?', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_enable_tfuse_seo_tab',
                            'value' => true,
                            'type' => 'checkbox',
                            'on_update' => 'reload_page',
                            'divider' => true
                        ),
                        // Enable Dynamic Image Resizer Option
                        array('name' => __('Dynamic Image Resizer', 'tfuse'),
                            'desc' => __('This will Enable the thumb.php script that dynamicaly resizes images on your site. We recommend you keep this enabled, however note that for this to work you need to have "GD Library" installed on your server. This should be done by your hosting server administrator.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_disable_resize',
                            'value' => 'true',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        array('name' => __('Image from content', 'tfuse'),
                            'desc' => __('If no thumbnail is specified then the first uploaded image in the post is used.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_enable_content_img',
                            'value' => 'false',
                            'type' => 'checkbox',
                            'divider' => true
                        ),
                        // Remove wordpress versions for security reasons
                        array(
                            'name' => __('Remove Wordpress Versions', 'tfuse'),
                            'desc' => __('Remove Wordpress versions from the source code, for security reasons.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_remove_wp_versions',
                            'value' => TRUE,
                            'type' => 'checkbox'
                        )
                    )
                ),
                array(
                    'name' => __('WordPress Admin Style', 'tfuse'),
                    'options' => array(
                        // Disable Themefuse Style
                        array('name' => __('Themefuse Style', 'tfuse'),
                            'desc' => __('Enable Themefuse Style', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_activate_tfuse_style',
                            'value' => true,
                            'type' => 'checkbox',
                            'on_update' => 'reload_page'
                        )
                    )
                )
            )
        ),
        array(
            'name' => __('Homepage', 'tfuse'),
            'id' => TF_THEME_PREFIX . '_homepage',
            'headings' => array(
                array(
                    'name' => __('Homepage Population', 'tfuse'),
                    'options' => array(
                        array('name' => __('Homepage Population', 'tfuse'),
                            'desc' => ' Select which categories to display on homepage. More over you can choose to load a specific page or change the number of posts on the homepage from <a target="_blank" href="' . network_admin_url('options-reading.php') . '">here</a>',
                            'id' => TF_THEME_PREFIX . '_homepage_category',
                            'value' => '',
                            'options' => array('all' => __('From All Categories','tfuse'), 'specific' => __('From Specific Categories','tfuse'),'page' =>__('From Specific Page', 'tfuse')),
                            'type' => 'select',
                            'install' => 'cat'
                        ),
                        array(
                            'name' => __('Select specific categories to display on homepage', 'tfuse'),
                            'desc' => __('Pick one or more
                            categories by starting to type the category name.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_categories_select_categ',
                            'type' => 'multi',
                            'subtype' => 'category',
                        ),
                        // page on homepage
                        array('name' => __('Select Page', 'tfuse'),
                            'desc' => __('Select the page', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_home_page',
                            'value' => 'image',
                            'options' =>tfuse_list_page_options(),
                            'type' => 'select',
                        ),
                        array('name' => __('Use page options', 'tfuse'),
                            'desc' => __('Use page options', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_use_page_options',
                            'value' => 'false',
                            'type' => 'checkbox'
                        )
                    )
                ),
                array(
                    'name' => __('Homepage Header', 'tfuse'),
                    'options' => array(
                        // Element of Hedear
                        array('name' => __('Element of Hedear', 'tfuse'),
                            'desc' => __('Select type of element on the header.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_header_element',
                            'value' => 'without',
                            'options' => array('without' => __('Without Header Element', 'tfuse'),'slider' => __('Slider on Header', 'tfuse'),'title' => __('Title on Header', 'tfuse'),'video' => 'Video on Header','map' => __('Map on Header', 'tfuse')),
                            'type' => 'select',
                        ),
                        // Select Header Slider
                        $this->ext->slider->model->has_sliders() ?
                                array(
                            'name' => __('Slider', 'tfuse'),
                            'desc' => __('Select a slider for your post. The sliders are created on the', 'tfuse') . '<a href="' . admin_url( 'admin.php?page=tf_slider_list' ) . '" target="_blank">' . __('Sliders page', 'tfuse') . '</a>.',
                            'id' => TF_THEME_PREFIX . '_select_slider',
                            'value' => '',
                            'options' => $TFUSE->ext->slider->get_sliders_dropdown(),
                            'type' => 'select'
                                ) :
                                array(
                            'name' => __('Slider', 'tfuse'),
                            'desc' => '',
                            'id' => TF_THEME_PREFIX . '_select_slider',
                            'value' => '',
                            'html' => __('No sliders created yet. You can start creating one', 'tfuse') . '<a href="' . admin_url('admin.php?page=tf_slider_list') . '">' . __('here', 'tfuse') . '</a>.',
                            'type' => 'raw'
                                )
                        ,
                        //map on header
                       array(
                                'name'=>__('Adress', 'tfuse'),
                                'desc'=>__('Enter your adress.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_adress',
                                'value' => '',
                                'type' =>'text'
                        ),
                        array(
                                'name'=>__('Email', 'tfuse'),
                                'desc'=>__('Enter the email.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_email',
                                'value' => '',
                                'type' =>'text'
                        ),
                        array(
                                'name'=>__('Phone', 'tfuse'),
                                'desc'=>__('Enter the phone number.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_phone',
                                'value' => '',
                                'type' =>'text'
                        ),

                        array(
                            'name' => __('Map position', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_page_map',
                            'value' => '',
                            'type' => 'maps'
                        ),

                        //title/subtitle and image
                         array(
                                'name'=>__('Title', 'tfuse'),
                                'desc'=>__('Enter the title.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_title',
                                'value' => '',
                                'type' =>'text'
                        ),
                        array(
                                'name'=>__('Subtitle', 'tfuse'),
                                'desc'=>__('Enter subtitle.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_subtitle',
                                'value' => '',
                                'type' =>'text'
                        ),
                         array(
                                'name'=>__('Video URL', 'tfuse'),
                                'desc'=>__('Enter the url.', 'tfuse'),
                                'id'=> TF_THEME_PREFIX . '_header_video',
                                'value' => '',
                                'type' =>'text'
                        )
                    )
                ),
                array(
                    'name' => __('Homepage Shortcodes', 'tfuse'),
                    'options' => array(
                        // Top Shortcodes
                        array('name' => __('Shortcodes before Content', 'tfuse'),
                            'desc' => __('In this textarea you can input your prefered custom shotcodes.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_content_top',
                            'value' => '',
                            'type' => 'textarea'
                        ),
                        // Bottom Shortcodes
                        array('name' => __('Shortcodes after Content', 'tfuse'),
                            'desc' => __('In this textarea you can input your prefered custom shotcodes.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_content_bottom',
                            'value' => '',
                            'type' => 'textarea'
                        )
                    )
                )
            )
        ),
        array(
            'name' => __('Blog', 'tfuse'),
            'id' => TF_THEME_PREFIX . '_blogpage',
            'headings' => array(
                array(
                    'name' => __('Blog Page Population', 'tfuse'),
                    'options' => array(
                        // Select the Blog Page
                        array('name' => __('Select Blog Page', 'tfuse'),
                            'desc' => __('Select the blog page', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_blog_page',
                            'value' => 'image',
                            'options' => tfuse_list_page_options(),
                            'type' => 'select',
                            'divider' => true
                        ),
                        array('name' => __('Blog Page Population', 'tfuse'),
                            'desc' => ' Select which categories to display on blogpage. More over you can choose to load a specific page or change the number of posts on the blogpage from <a target="_blank" href="' . network_admin_url('options-reading.php') . '">here</a>',
                            'id' => TF_THEME_PREFIX . '_blogpage_category',
                            'value' => '',
                            'options' => array('all' => __('From All Categories','tfuse'), 'specific' => __('From Specific Categories','tfuse')),
                            'type' => 'select',
                            'install' => 'cat'
                        ),
                        array(
                            'name' => __('Select specific categories to display on blogpage', 'tfuse'),
                            'desc' => __('Pick one or more
                            categories by starting to type the category name.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_categories_select_categ_blog',
                            'type' => 'multi',
                            'subtype' => 'category',
                        )
                    )
                ),
                array(
                    'name' => __('Blog Page Header', 'tfuse'),
                    'options' => array(
                         // Element of Hedear
                            array('name' => __('Element of Hedear', 'tfuse'),
                                'desc' => __('Select type of element on the header.', 'tfuse'),
                                'id' => TF_THEME_PREFIX . '_header_element_blog',
                                'value' => 'without',
                                'options' => array('without' => __('Without Header Element', 'tfuse'),'slider' => __('Slider on Header', 'tfuse'),'title' => __('Title on Header', 'tfuse'),'video' => 'Video on Header','map' => __('Map on Header', 'tfuse')),
                                'type' => 'select',
                            ),
                            // Select Header Slider
                            $this->ext->slider->model->has_sliders() ?
                                    array(
                                'name' => __('Slider', 'tfuse'),
                                'desc' => __('Select a slider for your post. The sliders are created on the', 'tfuse') . '<a href="' . admin_url( 'admin.php?page=tf_slider_list' ) . '" target="_blank">' . __('Sliders page', 'tfuse') . '</a>.',
                                'id' => TF_THEME_PREFIX . '_select_slider_blog',
                                'value' => '',
                                'options' => $TFUSE->ext->slider->get_sliders_dropdown(),
                                'type' => 'select'
                                    ) :
                                    array(
                                'name' => __('Slider', 'tfuse'),
                                'desc' => '',
                                'id' => TF_THEME_PREFIX . '_select_slider_blog',
                                'value' => '',
                                'html' => __('No sliders created yet. You can start creating one', 'tfuse') . '<a href="' . admin_url('admin.php?page=tf_slider_list') . '">' . __('here', 'tfuse') . '</a>.',
                                'type' => 'raw'
                                    )
                            ,
                            //map on header
                           array(
                                    'name'=>__('Adress', 'tfuse'),
                                    'desc'=>__('Enter your adress.', 'tfuse'),
                                    'id'=> TF_THEME_PREFIX . '_header_adress_blog',
                                    'value' => '',
                                    'type' =>'text'
                            ),
                            array(
                                    'name'=>__('Email', 'tfuse'),
                                    'desc'=>__('Enter the email.', 'tfuse'),
                                    'id'=> TF_THEME_PREFIX . '_header_email_blog',
                                    'value' => '',
                                    'type' =>'text'
                            ),
                            array(
                                    'name'=>__('Phone', 'tfuse'),
                                    'desc'=>__('Enter the phone number.', 'tfuse'),
                                    'id'=> TF_THEME_PREFIX . '_header_phone_blog',
                                    'value' => '',
                                    'type' =>'text'
                            ),

                            array(
                                'name' => __('Map position', 'tfuse'),
                                'id' => TF_THEME_PREFIX . '_page_map_blog',
                                'value' => '',
                                'type' => 'maps'
                            ),

                            //title/subtitle and image
                             array(
                                    'name'=>__('Title', 'tfuse'),
                                    'desc'=>__('Enter the title.', 'tfuse'),
                                    'id'=> TF_THEME_PREFIX . '_header_title_blog',
                                    'value' => '',
                                    'type' =>'text'
                            ),
                            array(
                                    'name'=>__('Subtitle', 'tfuse'),
                                    'desc'=>__('Enter subtitle.', 'tfuse'),
                                    'id'=> TF_THEME_PREFIX . '_header_subtitle_blog',
                                    'value' => '',
                                    'type' =>'text'
                            ),
                             array(
                                    'name'=>__('Video URL', 'tfuse'),
                                    'desc'=>__('Enter the url.', 'tfuse'),
                                    'id'=> TF_THEME_PREFIX . '_header_video_blog',
                                    'value' => '',
                                    'type' =>'text'
                            )
                    )
                ),
                array(
                    'name' => __('Blog Page Shortcodes', 'tfuse'),
                    'options' => array(
                        
                        // Bottom Shortcodes
                        array('name' => __('Shortcodes after Content', 'tfuse'),
                            'desc' => __('In this textarea you can input your prefered custom shotcodes.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_blog_content_bottom',
                            'value' => '',
                            'type' => 'textarea'
                        )
                    )
                ),
            )
        ),
        array(
            'name' => __('Posts', 'tfuse'),
            'id' => TF_THEME_PREFIX . '_posts',
            'headings' => array(
                array(
                    'name' => __('Default Post Options', 'tfuse'),
                    'options' => array(
                        // Post Content
                        array('name' => __('Post Content', 'tfuse'),
                            'desc' => __('Select if you want to show the full content (use <em>more</em> tag) or the excerpt on posts listings (categories).', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_post_content',
                            'value' => 'excerpt',
                            'options' => array('excerpt' => __('The Excerpt', 'tfuse'), 'content' => __('Full Content', 'tfuse')),
                            'type' => 'select',
                            'divider' => true
                        ),
                        // Video Position
                        array('name' => __('Video Position', 'tfuse'),
                            'desc' => __('Select your preferred video alignment', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_video_position',
                            'value' => 'alignleft',
                            'type' => 'images',
                            'options' => array('alignleft' => array($url . 'left_off.png', __('Align to the left', 'tfuse')), 'alignright' => array($url . 'right_off.png', __('Align to the right', 'tfuse')))
                        ),
                        // Video Dimensions
                        array('name' => __('Video Resize (px)', 'tfuse'),
                            'desc' => __('These are the default width and height values. If you want to resize the video change the values with your own. If you input only one, the video will get resized with constrained proportions based on the one you specified.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_video_dimensions',
                            'value' => array(618, 270),
                            'type' => 'textarray'
                        )
                    )
                )
            )
        ),
        array(
            'name' => __('Footer', 'tfuse'),
            'id' => TF_THEME_PREFIX . '_footer',
            'headings' => array(
                array(
                    'name' => __('Footer Content', 'tfuse'),
                    'options' => array(
                        // Enable Footer Shortcodes
                        array('name' => __('Enable Footer Shortcodes', 'tfuse'),
                            'desc' => __('This will enable footer shortcodes.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_enable_footer_shortcodes',
                            'value' => '',
                            'type' => 'checkbox'
                        ),
                        array('name' => __('Copyright', 'tfuse'),
                            'desc' => __('Change  Footer Copyright', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_footer_left_copyright',
                            'value' => '',
                            'type' => 'text'
                        ),
                        
                        // Footer Shortcodes
                        array('name' => __('Footer Shortcodes', 'tfuse'),
                            'desc' => __('In this textarea you can input your prefered custom shotcodes.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_footer_shortcodes',
                            'value' => '',
                            'type' => 'textarea'
                        )
                    )
                )
            )
        ),
       array(
            'name' => __('Styles', 'tfuse'),
            'id' => TF_THEME_PREFIX . '_feader',
            'headings' => array(
                array(
                    'name' => __('Header & Footer Background', 'tfuse'),
                    'options' => array(
			 array('name' => __('Type of header', 'tfuse'),
                                    'desc' => __('Select type of header.', 'tfuse'),
                                    'id' => TF_THEME_PREFIX . '_header_type',
                                    'value' => 'default',
                                    'options' => array('default' => 'Gadient', 'no' => __('Background Image', 'tfuse')),
                                    'type' => 'select'
                            ),
                        array('name' => __('Header Image', 'tfuse'),
                            'desc' => __('Default header image for site.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_header_image',
                            'value' => '',
                            'type' => 'upload',
                        ),
                        array('name' => __('Background Repeat', 'tfuse'),
                                    'desc' => __('Select type of background repeat.', 'tfuse'),
                                    'id' => TF_THEME_PREFIX . '_header_image_repeat',
                                    'value' => 'no-repeat',
                                    'options' => array('no-repeat' => 'no-repeat', 'repeat' => 'repeat','repeat-x' => 'repeat-x','repeat-y' => 'repeat-y'),
                                    'type' => 'select'
                            ),
                             array('name' => __('First Background Color', 'tfuse'),
                            'desc' => __('First Background Color for header.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_header_bg',
                            'value' => '#9F8369',
                            'type' => 'colorpicker'
                        ),
                         array('name' => __('Second Background Color', 'tfuse'),
                            'desc' => __('Second Background Color for header.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_header_bg2',
                            'value' => '#deca97',
                            'type' => 'colorpicker',
                            'divider' => true
                        ),
                            array('name' => __('Type of footer', 'tfuse'),
                                    'desc' => __('Select type of footer.', 'tfuse'),
                                    'id' => TF_THEME_PREFIX . '_footer_type',
                                    'value' => 'default',
                                    'options' => array('default' => __('Default', 'tfuse'),'yes' => __('Same as header', 'tfuse'),'gradient' =>'Gradient','no' => __('Background Image', 'tfuse')),
                                    'type' => 'select'
                            ),
                            array('name' => __('Footer Image', 'tfuse'),
                                'desc' => __('Default footer image for site.', 'tfuse'),
                                'id' => TF_THEME_PREFIX . '_footer_image',
                                'value' => '',
                                'type' => 'upload',
                            ),
                            array('name' => __('Background Repeat', 'tfuse'),
                                    'desc' => __('Select type of background repeat.', 'tfuse'),
                                    'id' => TF_THEME_PREFIX . '_footer_image_repeat',
                                    'value' => 'no-repeat',
                                    'options' => array('no-repeat' => 'no-repeat', 'repeat' => 'repeat','repeat-x' => 'repeat-x','repeat-y' => 'repeat-y'),
                                    'type' => 'select'
                            ),
                             array('name' => __('First Background Color', 'tfuse'),
                            'desc' => __('First Background Color for footer.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_footer_bg',
                            'value' => '#9F8369',
                            'type' => 'colorpicker'
                        ),
                         array('name' => __('Second Background Color', 'tfuse'),
                            'desc' => __('Second Background Color for footer.', 'tfuse'),
                            'id' => TF_THEME_PREFIX . '_footer_bg2',
                            'value' => '#deca97',
                            'type' => 'colorpicker'
                        )
                    )
                )	
            )
        )
    )
);

?>