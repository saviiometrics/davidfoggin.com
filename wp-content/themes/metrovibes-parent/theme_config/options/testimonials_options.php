<?php

/* ----------------------------------------------------------------------------------- */
/* Initializes all the theme settings option fields for posts area. */
/* ----------------------------------------------------------------------------------- */

$options = array(
    /* ----------------------------------------------------------------------------------- */
    /* Sidebar */
    /* ----------------------------------------------------------------------------------- */


    /* ----------------------------------------------------------------------------------- */
    /* After Textarea */
    /* ----------------------------------------------------------------------------------- */

    /* Post Media */
    array('name' => __('Media', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_media',
        'type' => 'metabox',
        'context' => 'normal'
    ),
    // Subtitle
    array('name' => __('Image', 'tfuse'),
        'desc' => __('Upload author\'s avatar. ', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_img',
        'value' => '',
        'type' => 'upload'
    ),
    // Single Image Position
    array('name' => __('Image Position', 'tfuse'),
        'desc' => __('Select your preferred image  alignment', 'tfuse'),
        'id' => TF_THEME_PREFIX . '_test_img_position',
        'value' => tfuse_options('test_image_position'),
        'options' => array(
            'alignleft' => array($url . 'left_off.png', __('Align to the left', 'tfuse')),
            'alignright' => array($url . 'right_off.png', __('Align to the right', 'tfuse'))
        ),
        'type' => 'images'
    )
);

?>