<?php
if (!function_exists('tfuse_aaasort')) :
    /**
     *
     *
     * To override tfuse_aasort() in a child theme, add your own tfuse_aasort()
     * to your child theme's file.
     */
    function tfuse_aaasort ($array, $key) {
        $sorter=array();
        $ret=array();
        if (!$array){$array = array();}
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii]=$va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        return $ret;
    }
endif;

if (!function_exists('tfuse_shorten_string')) :
    /**
     * To override tfuse_shorten_string() in a child theme, add your own tfuse_shorten_string()
     * to your child theme's theme_config/theme_includes/THEME_FUNCTIONS.php file.
     */

function tfuse_shorten_string($string, $wordsreturned)

{
    $retval = $string;

    $array = explode(" ", $string);
    if (count($array)<=$wordsreturned)

    {
        $retval = $string;
    }
    else

    {
        array_splice($array, $wordsreturned);
        $retval = implode(" ", $array)." ...";
    }
    return $retval;
}

endif;

if (!function_exists('tfuse_rewrite_worpress_reading_options')):

    /**
     *
     *
     * To override tfuse_rewrite_worpress_reading_options() in a child theme, add your own tfuse_rewrite_worpress_reading_options()
     * to your child theme's file.
     */

    add_action('tfuse_admin_save_options','tfuse_rewrite_worpress_reading_options', 10, 1);

    function tfuse_rewrite_worpress_reading_options ($options)
    {
        if($options[TF_THEME_PREFIX . '_homepage_category'] == 'page')
        {
            update_option('show_on_front', 'page');
            update_option('page_on_front', intval($options[TF_THEME_PREFIX . '_home_page']));
        }
        else
        {
            update_option('show_on_front', 'posts');
            update_option('page_on_front', 0);
        }
    }
endif;

if (!function_exists('tfuse_ajax_get_posts')) :
    function tfuse_ajax_get_posts(){  
        $max = (intval($_POST['max'])); 
        $pageNum = (intval($_POST['pageNum']));
        $search_key = $_POST['search_key'];
        $allhome = $_POST['allhome'];
        $allblog = $_POST['allblog'];
        $homepage = $_POST['homepage'];
        $cat_ids = $_POST['cat_ids'];           
        $cat_ID = (intval($_POST['id']));
        $is_tax = $_POST['is_tax']; 
        $sidebar_exist = $_POST['sidebar_exist'];
        $items = get_option('posts_per_page');  
        
        $pos4 = $pos1 = $pos2= $pos3 =$pos5 = $pos6 = $pos8 = $allpos = $pos  = '';
        $posts = array();
    if($pageNum <= $max) {
        if($homepage == 'homepage' && $allhome == 'nonehomeall')
        {  
            $specific = tfuse_options('categories_select_categ'); 
            if(is_user_logged_in())
            {
                $args = array(
                    'post_status' => array( 'publish','private' ) ,
                            'paged' => $pageNum,
                            'cat' => $specific
                );
            }
            else 
            {
                $args = array(
                    'post_status' => array( 'publish') ,
                            'paged' => $pageNum,
                            'cat' => $specific
                );
            }
            $query = new WP_Query($args);
            $posts = $query->posts; 
        }
        elseif($homepage == 'blogpage' && $allblog == 'allblogcategories')
        { 
            
                    $args = array(
                        'paged' => $pageNum,
                        'post_type' => 'post'
                    );
                $query = new WP_Query( $args );
                $posts = $query -> posts; 
        }
        elseif($homepage == 'blogpage' && $allblog == 'noneblogall')
        { 
            $specific = tfuse_options('categories_select_categ_blog'); 
            if(is_user_logged_in())
            {
                $args = array(
                    'post_status' => array( 'publish','private' ) ,
                    'paged' => $pageNum,
                    'cat' => $specific
                );
            }
            else
            {
                $args = array(
                    'post_status' => array( 'publish' ) ,
                    'paged' => $pageNum,
                    'cat' => $specific
                );
            }
            $query = new WP_Query( $args );
            $posts = $query -> posts; 
        }
        elseif($is_tax == 'category')
        { 
            if($cat_ID == 0)
            {  
                if(is_user_logged_in())
                {
                    $args = array(
                        'post_status' => array( 'publish','private' ) ,
                                'paged' => $pageNum,
                                'cat' => $cat_ids
                    );
                }
                else 
                {
                    $args = array(
                        'post_status' => array( 'publish') ,
                                'paged' => $pageNum,
                                'cat' => $cat_ids
                    );
                }
                $query = new WP_Query($args);
                $posts = $query->posts; 
            }
            else
            {
                if(is_user_logged_in())
                {
                    $query = new WP_Query(array('post_status' => array( 'publish','private' ) , 'cat' => $cat_ID,'paged' => $pageNum));
                }
                else
                {
                    $query = new WP_Query(array('post_status' => array( 'publish' ) , 'cat' => $cat_ID,'paged' => $pageNum));
                }
            }
            $posts = $query->posts;
        }
        elseif(($homepage == 'homepage') && ($allhome == 'allhomecategories'))
        { 
            if(is_user_logged_in())
            {
                $args = array(
                    'post_status' => array( 'publish','private' ) ,
                            'paged' => $pageNum,
                            'cat' => $cat_ids
                );
            }
            else 
            {
                $args = array(
                    'post_status' => array( 'publish') ,
                            'paged' => $pageNum,
                            'cat' => $cat_ids
                );
            }
            $query = new WP_Query($args);
            $posts = $query->posts; 
        }
        
        elseif($is_tax == 'yes')
        { 
            if($cat_ID == 0)
            {
                if(is_user_logged_in())
                {
                    $args = array(
                            'post_status' => array( 'publish','private' ),
                            'post_type' => 'portfolio',
                            'paged' => $pageNum
                    );
                }
                else 
                {
                    $args = array(
                            'post_status' => array( 'publish'),
                            'post_type' => 'portfolio',
                            'paged' => $pageNum
                    );
                }
                $query = new WP_Query($args);
                $posts = $query->posts;
            }
            else
            {
                if(is_user_logged_in())
                {
                    $args = array(
                        'post_status' => array( 'publish','private' ),
                            'paged' => $pageNum,
                            'tax_query' => array(
                                    array(
                                            'taxonomy' => 'group',
                                            'field' => 'id',
                                            'terms' => $cat_ID
                                    )
                            )
                    );
                }
                else
                {
                    $args = array(
                        'post_status' => array( 'publish' ),
                            'paged' => $pageNum,
                            'tax_query' => array(
                                    array(
                                            'taxonomy' => 'group',
                                            'field' => 'id',
                                            'terms' => $cat_ID
                                    )
                            )
                    );
                }
               $query = new WP_Query( $args );
               $posts = $query->posts;
            }
        }
        elseif($is_tax == 'service')
        {
            if(is_user_logged_in())
            {
                $args = array(
                        'post_status' => array( 'publish','private' ),
                        'post_type' => 'service',
                        'paged' => $pageNum
                );
            }
            else 
            {
                $args = array(
                        'post_status' => array( 'publish'),
                        'post_type' => 'service',
                        'paged' => $pageNum
                );
            }
            $query = new WP_Query($args);
            $posts = $query->posts;
        }
        elseif($is_tax == 'search') 
        { 
            if(is_user_logged_in())
            {
                $query = new WP_Query(array('post_status' => array( 'publish','private' ) , 's' => $search_key ,'paged' => $pageNum));
            }
            else
            {
                $query = new WP_Query(array('post_status' => array( 'publish' ) , 's' => $search_key ,'paged' => $pageNum));
            }
            $posts = $query->posts;
        }
        
        
        $cnt = 0; 
        $type = tfuse_options('portf_type');
        foreach($posts as $post)
        { 
            $posts[] = $post->ID;
            $cnt++;
            $autor_id = $post->post_author;
            $author_name = get_userdata($autor_id)->display_name;
            if($sidebar_exist == 'no' && ($cnt % 3 == 0)) $pos7 .= '<div class="clear"></div>';
            elseif($sidebar_exist == 'yes' && ($cnt % 2 == 0)) $pos7 .= '';
            
            if($is_tax == 'yes')
            {
                $k = 0;
                $attachments = tfuse_get_gallery_images($post->ID,TF_THEME_PREFIX . '_thumbnail_image');
                $slider_images = array();
                    if ($attachments) {
                        foreach ($attachments as $attachment){
                            $slider_images[] = array(
                                'order'        =>$attachment->menu_order,
                                'img_full'    => $attachment->guid,
                            );
                        }
                    }
                $slider_images = tfuse_aaasort($slider_images,'order');
                $pos1 .= '<div class="gallery-item clearfix">
                            <div class="gallery-image">';
                        foreach($slider_images as $slider): $k++;
                            if($k == 1){ 
                                if($type == 'pretty'){
                                $pos2 .= '<a href="'.$slider["img_full"].'" class="preload" id="ajax'.$post->ID.'" data-rel="prettyPhoto['.$post->ID.']" title="'.$post->post_title .'">
                                    <img src="' . TF_GET_IMAGE::get_src_link($slider['img_full'], 300, 300) . '" alt="">
                                </a>';
                                }else{
                                    $pos2 .= '<a href="'.get_permalink( $post->ID ).'" class="preload" id="ajax'.$post->ID.'" title="'.$post->post_title .'">
                                    <img src="' . TF_GET_IMAGE::get_src_link($slider['img_full']) . '" alt="">
                                </a>';
                                }
                            }
                            else { 
                                $pos3.=' <a href="'.$slider["img_full"].'" class="hidden" data-rel="prettyPhoto['.$post->ID.']"></a>';
                            }
                            if($type == 'single' && $k == 1) break;
                        endforeach;
                $pos4 .= '  </div>
                        </div>';
                  
                  $pos = $pos1.$pos2.$pos3.$pos4;
                  $pos1  = $pos2 = $pos3 = $pos4 = '';
            }
            elseif($is_tax == 'category')
            { 
                $img = tfuse_page_options('thumbnail_image',null,$post->ID);
                if(tfuse_options('disable_listing_lightbox'))
                {
                    $image = '<a href="'.$img.'" rel="prettyPhoto[gallery'.$post->ID.']">
                                <img src="'.$img.'"  height="210" alt="">
                            </a>';
                }
                else
                {$image = '<a href="'.get_permalink( $post->ID ).'"><img src="'.$img.'"  height="210" alt=""></a>';}
                $views = get_post_meta($post->ID, TF_THEME_PREFIX . '_post_viewed', true);
                $views = intval($views);
                $margin = '';
                $thumb = tfuse_page_options('thumbnail_image','',$post->ID);
                if(empty($thumb)) $margin = 'style="padding-top:60px"';
                $pos5 .='<div class="post-item ">
                            <div class="post-image">'.$image.'</div>
                            <div class="post-title" '.$margin.'>
                                <h2><a href="'.get_permalink( $post->ID ).'">'.$post->post_title.'</a></h2>
                            </div>
                            <div class="post-meta-top">';
                            if ( tfuse_options('date_time')) :
                                $pos6 .='<div class="post-date">
                                    <span class="date">'.get_the_time( 'j', $post->ID ).'</span>
                                    <span class="month">'.get_the_time( 'M', $post->ID ).'</span>
                                </div>';
                            endif;
                $pos8 .='</div>
                            <div class="post-desc clearfix">
                                '.strip_tags(tfuse_shorten_string(apply_filters('the_content',$post->post_content),50)).'
                            </div>
                            <div class="post-meta-bot clearfix">
                                <div class="post-meta post-view"><span><i class="view-ico"></i>'.$views.'</span></div>
                                <div class="post-meta post-comm"><span><i class="comment-ico"></i>'.$post->comment_count.'</span></div>
                                <div class="post-meta post-author"><span><i class="pencil-ico"></i>'.$author_name.'</span></div>
                                <div class="post-meta post-read"><a href="'.get_permalink( $post->ID ).'" class="text-center "><i class="read-ico"></i><span>'.__(' Read More','tfuse').'</span></a></div>
                            </div>
                        </div>';
                            $pos = $pos5.$pos6.$pos8;
                            $pos5 = $pos6 = $pos8 = '';
            }
            elseif($is_tax == 'service')
            {
                $service_link = tfuse_options('service_link');
                if($service_link != 'link') $link = get_permalink($post->ID);
                else $link = tfuse_page_options('service_custom_link','',$post->ID);
                $pos1 .= '<div class="service_item">
                            <div class="service_img"><img src="'.tfuse_page_options('thumbnail_image','',$post->ID).'" width="112" height="112" alt=""></div>
                            <div class="service_title"><h2>'.$post->post_title .'</h2></div>
                            <div class="service_desc">
                                 <p>'.strip_tags(tfuse_shorten_string(apply_filters('the_content',$post->post_content),30)).'</p>
                            </div> 
                            <div class="service_meta_bot"><a href="'.$link.'" class="link-more">'. __('Related works','tfuse').'</a></div>
                        </div>';
                $pos = $pos1.$pos7;
                $pos1  = $pos7 = ''; 
            }
                $allpos[] = $pos;
        }
        $rsp = array('html'=> $allpos,'items' => $items,'posts'=> $posts); 
        echo json_encode($rsp);
    }
        die();
    }
    add_action('wp_ajax_tfuse_ajax_get_posts','tfuse_ajax_get_posts');
    add_action('wp_ajax_nopriv_tfuse_ajax_get_posts','tfuse_ajax_get_posts');
endif;

if (!function_exists('tfuse_ajax_get_service_link')) :
    function tfuse_ajax_get_service_link(){        
         $service_link = tfuse_options('service_link');
         if($service_link != 'link')
             $link = 'no';
         else
             $link = 'yes';
        echo $link;
        die();
    }
    add_action('wp_ajax_tfuse_ajax_get_service_link','tfuse_ajax_get_service_link');
    add_action('wp_ajax_nopriv_tfuse_ajax_get_service_link ','tfuse_ajax_get_service_link');
endif;