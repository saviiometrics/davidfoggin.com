<?php

if ( ! function_exists( 'tfuse_header_content' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override tfuse_slider_type() in a child theme, add your own tfuse_slider_type to your child theme's
 * functions.php file.
 */

    function tfuse_header_content($location = false)
    { 
        global $TFUSE, $post,$is_tf_blog_page,$header_title,$header_map,$header_video;
        $posts = $header_element = $slider = $header_video = $header_title = $header_map = null;
		if(!empty($post)) $posttype = $post->post_type;  else $posttype = '';
        $filter =  $TFUSE->request->isset_GET('posts') ? $TFUSE->request->GET('posts') : "";
        if (!$location) return;
        switch ($location)
        { 
            case 'header' :
                if ( $posttype == 'post' && $filter == 'all')
                { 
                    $header_element = tfuse_options('header_element_posts');
                    if ( 'slider' == $header_element )
                        $slider = tfuse_options('select_slider_posts');
                    elseif ( 'title' == $header_element )
                    {
                        $header_title['title'] = tfuse_options('header_title_posts');
                        $header_title['subtitle'] = tfuse_options('header_subtitle_posts');
                    }
                    elseif ( 'video' == $header_element )
                    {
                        $header_video['video'] = tfuse_options('header_video_posts');
                    }
                    elseif ( 'map' == $header_element )
                    {
                        $header_map['email'] = tfuse_options('header_email_posts');
                        $header_map['phone'] = tfuse_options('header_phone_posts');
                        $header_map['adress'] = tfuse_options('header_adress_posts');
                    }
                }
                elseif ( $posttype == 'portfolio' && $filter == 'all')
                { 
                    $header_element = tfuse_options('header_element_portf');
                    if ( 'slider' == $header_element )
                        $slider = tfuse_options('select_slider_portf');
                    elseif ( 'title' == $header_element )
                    {
                        $header_title['title'] = tfuse_options('header_title_portf');
                        $header_title['subtitle'] = tfuse_options('header_subtitle_portf');
                    }
                    elseif ( 'video' == $header_element )
                    {
                        $header_video['video'] = tfuse_options('header_video_portf');
                    }
                    elseif ( 'map' == $header_element )
                    {
                        $header_map['email'] = tfuse_options('header_email_portf');
                        $header_map['phone'] = tfuse_options('header_phone_portf');
                        $header_map['adress'] = tfuse_options('header_adress_portf');
                    }
                }
                elseif(is_front_page())
                {
					if(!empty($post))$ID = $post->ID;  else $ID = '';
					
                    $page_id = tfuse_options('home_page');
                    $header_element = tfuse_options('header_element');
                    if(tfuse_options('use_page_options') && tfuse_options('homepage_category')=='page')
                    {   $header_element = tfuse_page_options('header_element','',$page_id);
                        if($page_id != 0 && tfuse_page_options('header_element','',$page_id)=='slider')
                            $slider = tfuse_page_options('select_slider','',$page_id);
                        elseif ( $page_id != 0 && tfuse_page_options('header_element','',$page_id) == 'video')
                        {
                            $header_video['video'] = tfuse_page_options('header_video','',$page_id);
                        }
                        elseif ($page_id != 0 && tfuse_page_options('header_element','',$page_id)== 'title')
                        {
                            $header_title['title'] = tfuse_page_options('header_title','',$page_id);
                            $header_title['subtitle'] = tfuse_page_options('header_subtitle','',$page_id);
                        }
                        elseif ($page_id != 0 && tfuse_page_options('header_element','',$page_id) == 'map')
                        {  
                            $header_map['email'] = tfuse_page_options('header_email','',$page_id);
                            $header_map['phone'] = tfuse_page_options('header_phone','',$page_id);
                            $header_map['adress'] = tfuse_page_options('header_adress','',$page_id);
                        }
                    }
                    else{
                        if ( 'slider' == $header_element )
                            $slider = tfuse_options('select_slider');
                        elseif ( 'title' == $header_element )
                        {
                            $header_title['title'] = tfuse_options('header_title');
                            $header_title['subtitle'] = tfuse_options('header_subtitle');
                        }
                        elseif ( 'video' == $header_element )
                        {
                            $header_video['video'] = tfuse_options('header_video');
                        }
                        elseif ( 'map' == $header_element )
                        {
                            $header_map['email'] = tfuse_options('header_email');
                            $header_map['phone'] = tfuse_options('header_phone');
                            $header_map['adress'] = tfuse_options('header_adress');
                        }
                    }
                    
                }
                elseif($is_tf_blog_page)
                { 
                    $ID = $post->ID;
                    $header_element = tfuse_options('header_element_blog');
                    if ( 'slider' == $header_element )
                            $slider = tfuse_options('select_slider_blog');
                    elseif ( 'title' == $header_element )
                    {
                        $header_title['title'] = tfuse_options('header_title_blog');
                        $header_title['subtitle'] = tfuse_options('header_subtitle_blog');
                    }
                    elseif ( 'video' == $header_element )
                    {
                        $header_video['video'] = tfuse_options('header_video_blog');
                    }
                    elseif ( 'map' == $header_element )
                    {
                        $header_map['email'] = tfuse_options('header_email_blog');
                        $header_map['phone'] = tfuse_options('header_phone_blog');
                        $header_map['adress'] = tfuse_options('header_adress_blog');
                    }
                }
                elseif ( is_singular() )
                {  
                    $ID = $post->ID;
                    $header_element = tfuse_page_options('header_element');
                    if ( 'slider' == $header_element )
                        $slider = tfuse_page_options('select_slider');
                    elseif ( 'title' == $header_element )
                    {
                        $header_title['title'] = tfuse_page_options('header_title');
                        $header_title['subtitle'] = tfuse_page_options('header_subtitle');
                    }
                    elseif ( 'video' == $header_element )
                    {
                        $header_video['video'] = tfuse_page_options('header_video');
                    }
                    elseif ( 'map' == $header_element )
                    {
                        $header_map['email'] = tfuse_page_options('header_email');
                        $header_map['phone'] = tfuse_page_options('header_phone');
                        $header_map['adress'] = tfuse_page_options('header_adress');
                    }

                }
                elseif ( is_category() )
                { 
                    $ID = get_query_var('cat');
                    $header_element = tfuse_options('header_element', null, $ID);
                    if ( 'slider' == $header_element )
                        $slider = tfuse_options('select_slider', null, $ID);
                    elseif ( 'title' == $header_element )
                    {
                        $header_title['title'] = tfuse_options('header_title', null, $ID);
                        $header_title['subtitle'] = tfuse_options('header_subtitle', null, $ID);
                    }
                    elseif ( 'video' == $header_element )
                    {
                        $header_video['video'] = tfuse_options('header_video', null, $ID);
                    }
                    elseif ( 'map' == $header_element )
                    {
                        $header_map['email'] = tfuse_options('header_email', null, $ID);
                        $header_map['phone'] = tfuse_options('header_phone', null, $ID);
                        $header_map['adress'] = tfuse_options('header_adress', null, $ID);
                    }

                }
                elseif ( is_tax() )
                { 
                    $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
                    $ID = $term->term_id;
                    $header_element = tfuse_options('header_element', null, $ID);
                    if ( 'slider' == $header_element )
                        $slider = tfuse_options('select_slider', null, $ID);
                    elseif ( 'title' == $header_element )
                    {
                        $header_title['title'] = tfuse_options('header_title', null, $ID);
                        $header_title['subtitle'] = tfuse_options('header_subtitle', null, $ID);
                    }
                    elseif ( 'video' == $header_element )
                    {
                        $header_video['video'] = tfuse_options('header_video', null, $ID);
                    }
                    elseif ( 'map' == $header_element )
                    {
                        $header_map['email'] = tfuse_options('header_email', null, $ID);
                        $header_map['phone'] = tfuse_options('header_phone', null, $ID);
                        $header_map['adress'] = tfuse_options('header_adress', null, $ID);
                    }

                }
                
                
            break;
        }  
        if ( $header_element == 'map' )
        { 
            get_template_part( 'header', 'map' );
            return;
        }
         elseif( $header_element == 'title')
        {   
            get_template_part( 'header', 'title');
            return;
        }
         elseif( $header_element == 'video')
        {   
            get_template_part( 'header', 'video');
            return;
        }
        elseif ( !$slider )
            return;

        $slider = $TFUSE->ext->slider->model->get_slider($slider);

        switch ($slider['type']):
           case 'custom':

                if ( is_array($slider['slides']) ) :
                    $slider_image_resize = ( isset($slider['general']['slider_image_resize']) && $slider['general']['slider_image_resize'] == 'true' ) ? true : false;
                    foreach ($slider['slides'] as $k => $slide) : 
                        $image = new TF_GET_IMAGE();
                        if ( $slider['design'] == 'onebyone')
                        { 
                            $slider['slides'][$k]['slide_src'] = $image->width(940)->height(449)->src($slide['slide_src'])->resize($slider_image_resize)->get_src();
                        }
                        else
                        { 
                            @$slider['slides'][$k]['slide_src'] = $image->width(426)->height(240)->src($slide['slide_src'])->resize($slider_image_resize)->get_src();
                        }
                    endforeach;
                endif;

                break;
            case 'posts':
                $slides_posts = array();
                
                    $args = array( 'post__in' => explode(',',$slider['general']['posts_select']) );
                    $slides_posts = explode(',',$slider['general']['posts_select']);
                
                foreach($slides_posts as $slide_posts):
                    $posts[] = get_post($slide_posts);
                endforeach; 
                $posts = array_reverse($posts);
                $args = apply_filters('tfuse_slider_posts_args', $args, $slider);
                $args = apply_filters('tfuse_slider_posts_args_'.$ID, $args, $slider);
                break;
            case 'categories':
                    $args = 'cat='.$slider['general']['posts_select_cat'].
                        '&posts_per_page='.$slider['general']['sliders_posts_number'];
                    $args = apply_filters('tfuse_slider_categories_args', $args, $slider);
                    $args = apply_filters('tfuse_slider_categories_args_'.$ID, $args, $slider);
                    $posts = get_posts($args);
                break;

        endswitch;

        if ( is_array($posts) ) :
            $slider['slides'] = tfuse_get_slides_from_posts($posts,$slider);
        endif;

        if ( !is_array($slider['slides']) ) return;

        include_once(locate_template( '/theme_config/extensions/slider/designs/'.$slider['design'].'/template.php' ));
    }

endif;
add_action('tfuse_header_content', 'tfuse_get_header_content');

if ( ! function_exists( 'tfuse_get_slides_from_posts' ) ):
/**
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override tfuse_slider_type() in a child theme, add your own tfuse_slider_type to your child theme's
 * functions.php file.
 */
    function tfuse_get_slides_from_posts( $posts=array(), $slider = array() )
    {
        global $post;

        $slides = array();
        $slider_image_resize = ( isset($slider['general']['slider_image_resize']) && $slider['general']['slider_image_resize'] == 'true' ) ? $slider['general']['slider_image_resize'] : false;
        $k = 0;
        foreach ($posts as $k => $post) : $k++;
            setup_postdata( $post );
        
        $tfuse_image = $image = null;
            if ($post->post_type == 'post'){
                
                $thumbnail = tfuse_page_options('thumbnail_image');
                $single = tfuse_page_options('single_image');
                if(empty($thumbnail) && empty($single)) continue;
                elseif(empty($thumbnail) && !empty ($single)) $single_image = $single;
                else $single_image = $thumbnail;
                    
                $image = new TF_GET_IMAGE();
                $tfuse_image = $image->width(426)->height(240)->src($single_image)->resize($slider_image_resize)->get_src();
                
                if($k % 2 == 0) $slides[$k]['slide_align_img'] = 'alignright';
                else $slides[$k]['slide_align_img'] = 'alignleft';
                $title = get_the_title();
                if (mb_strlen($title, 'UTF-8') > 40)  $title = substr($title, 0 ,40) . '...';
                $slides[$k]['slide_title'] = $title;
                $slides[$k]['slide_src'] = $tfuse_image;
                $slides[$k]['slide_url'] = get_permalink();
                $slides[$k]['slide_description'] = tfuse_substr( strip_tags(apply_filters('the_content',$post->post_content)),200);
            }
            elseif($post->post_type == 'service')
            { 
                $single = tfuse_options('service_link');
                if($single == 'single') $slides[$k]['slide_url'] = get_permalink();
                else $slides[$k]['slide_url'] = tfuse_page_options('service_custom_link','',$post->ID);
                        
                $thumbnail = tfuse_page_options('thumbnail_image');
                if(empty($thumbnail) ) continue;
                
                $title = get_the_title();
                if (mb_strlen($title, 'UTF-8') > 40)  $title = substr($title, 0 ,40) . '...';
                $slides[$k]['title'] = $title;
                $slides[$k]['img_src'] = $thumbnail;
                $slides[$k]['description'] = tfuse_substr( strip_tags(apply_filters('the_content',$post->post_content)),140);
                if($k == 7) break;
            }
            else
            {
                $attachments = tfuse_get_gallery_images($post->ID,TF_THEME_PREFIX . '_thumbnail_image');
                $slider_images = array();
                    if ($attachments) {
                        foreach ($attachments as $attachment){
                            $slider_images[] = array(
                                'order'        =>$attachment->menu_order,
                                'img_full'    => $attachment->guid,
                            );
                        }
                    }
                    $slider_images = tfuse_aasort($slider_images,'order');
                foreach($slider_images as $slider):
                    if ( empty( $slider['img_full']) ) continue;
                    $tfuse_image = $slider['img_full']; break;
                endforeach;
                $slides[$k]['slide_src'] = $tfuse_image;
                $slides[$k]['slide_url'] = get_permalink();
                if($k == 7) break;
            }
        endforeach;

        return $slides;
    }
endif;
