<?php
/**
 * The template for displaying posts on archive pages.
 * To override this template in a child theme, copy this file 
 * to your child theme's folder.
 *
 * @since MetroVibes 1.0
 */ 
global $post;
$service_link = tfuse_options('service_link');
if($service_link != 'link') $link = get_permalink($post->ID);
else $link = tfuse_page_options('service_custom_link');
 global $more;
    $more = apply_filters('tfuse_more_tag',0);
?>
<!--service item-->	
<div class="service_item">
    <div class="service_img"><img src="<?php echo tfuse_page_options('thumbnail_image');?>" width="112" height="112" alt=""></div>
    <div class="service_title"><h2><?php  tfuse_custom_title(); ?></h2></div>
    <div class="service_desc">
        <?php if ( tfuse_options('post_content') == 'content' ) the_content(''); else the_excerpt(); ?>
    </div>
    <div class="service_meta_bot"><a href="<?php echo $link; ?>" class="link-more"><?php _e('Related works','tfuse');?></a></div>
</div>
<!--/service item-->