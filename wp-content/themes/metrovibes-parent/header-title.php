<?php 
    global $header_title;
    if ( !empty($header_title['title'])) :
?>
<!-- header -->
<div class="header header_thin" <?php echo tfuse_change_header_bg();?>>
    <div class="container">
        <div class="header-title">
            <h1><?php echo $header_title['title'];?></h1>
            <h5><?php echo $header_title['subtitle'];?></h5>
        </div>
    </div>
</div>
<!--/ header -->
<?php endif;?>