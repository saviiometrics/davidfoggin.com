<?php
/**
 * The template for displaying content in the single.php template.
 * To override this template in a child theme, copy this file 
 * to your child theme's folder.
 *
 * @since MetroVibes 1.0
 */
global $post;
$views = get_post_meta($post->ID, TF_THEME_PREFIX . '_post_viewed', true);
$views = intval($views);
$classes = '';
$single = tfuse_page_options('single_image');
if ( tfuse_page_options('disable_published_date',tfuse_options('disable_published_date')) && tfuse_options('date_time'))
    $show_published_date = true;
if (empty($single) && !empty($show_published_date)) $classes = ' tf_show_published_date';
?>
<div class="post-image" ><?php echo tfuse_media($return=false,$type = '');?></div>
<div class="post-title<?php echo $classes; ?>"><h2><?php tfuse_custom_title();?></h2></div>
<div class="post-meta-top">
<?php if ( !empty($show_published_date) ) : ?>
<div class="post-date">
        <span class="date"><?php echo get_the_date('j'); ?></span>
        <span class="month"><?php echo get_the_date('M'); ?></span>
</div>
 <?php endif; ?>
</div>
<div class="post-desc clearfix">
     <?php the_content(); ?>
</div>
<div class="post_pagination"><?php wp_link_pages(); ?></div>
<?php if ( tfuse_page_options('disable_author_info',tfuse_options('disable_author_info')) ) : ?>
    <!-- author description -->
        <?php get_template_part('content','author');?>
    <!--/ author description -->
<?php endif; ?>
<?php if ( tfuse_page_options('disable_post_meta',tfuse_options('disable_post_meta'))) : ?>
    <div class="post-meta-bot clearfix">
        <?php if ( tfuse_page_options('disable_views')) : ?>
            <div class="post-meta post-view"><span><i class="view-ico"></i><?php echo $views;?></span></div>
        <?php endif; ?>
        <?php if ( tfuse_page_options('disable_comments',tfuse_options('disable_posts_comments'))  ) : ?>
            <div class="post-meta post-comm"><span><i class="comment-ico"></i><?php comments_number("0", "1", "%"); ?></span></div>
        <?php endif; ?>
        <?php if ( tfuse_page_options('disable_author_info',tfuse_options('disable_author_info')) ) : ?>
            <div class="post-meta post-author"><span><i class="pencil-ico"></i><?php echo get_the_author(); ?></span></div>
        <?php endif; ?>
        <?php if ( tfuse_page_options('disable_published_date',tfuse_options('disable_published_date'))  && tfuse_options('date_time')) : ?>
            <div class="post-meta post-read"><span><i class="calendar-ico"></i><?php echo get_the_date(); ?></span></div>
        <?php endif; ?>
    </div>
<?php endif; ?>

            