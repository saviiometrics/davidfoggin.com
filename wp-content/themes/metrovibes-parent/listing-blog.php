<?php
/**
 * The template for displaying posts on archive pages.
 * To override this template in a child theme, copy this file 
 * to your child theme's folder.
 *
 * @since MetroVibes 1.0
 */
global $post;
$views = get_post_meta($post->ID, TF_THEME_PREFIX . '_post_viewed', true);
$views = intval($views);
$classes = '';
$thumb = tfuse_page_options('thumbnail_image');
if ( tfuse_page_options('disable_published_date',tfuse_options('disable_published_date')) && tfuse_options('date_time'))
    $show_published_date = true;
if (empty($thumb) && !empty($show_published_date)) $classes = ' tf_show_published_date2';
global $more;
$more = apply_filters('tfuse_more_tag',0);
?>
<div class="post-item clearfix">
    <div class="post-image"><?php echo tfuse_media($return=false,$type = 'blog');?></div>
    <div class="post-title<?php echo $classes; ?>"><h2><a href="<?php the_permalink(); ?>"> <?php  tfuse_custom_title(); ?></a></h2></div>
    <div class="post-meta-top">
    <?php if ( $show_published_date ) : ?>
        <div class="post-date">
            <span class="date"><?php echo get_the_date('j'); ?></span>
            <span class="month"><?php echo get_the_date('M'); ?></span>
        </div>
    <?php endif; ?>
    </div>
    <div class="post-desc clearfix">
        <?php if ( tfuse_options('post_content') == 'content' ) the_content(''); else the_excerpt(); ?>
    </div>
    <div class="post-meta-bot clearfix">
        <div class="post-meta post-view"><span><i class="view-ico"></i><?php echo $views;?></span></div>
        <div class="post-meta post-comm"><span><i class="comment-ico"></i><?php comments_number("0", "1", "%"); ?></span></div>
        <div class="post-meta post-author"><span><i class="pencil-ico"></i><?php echo get_the_author(); ?></span></div>
        <div class="post-meta post-read"><a href="<?php the_permalink(); ?>" class="text-center "><i class="read-ico"></i><span><?php _e(' Read More','tfuse'); ?></span></a></div>
    </div>
</div>
     