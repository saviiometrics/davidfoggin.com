<?php get_header(); ?>
<?php $sidebar_position = tfuse_sidebar_position(); ?>
<div class="header-bottom">
    <div class="container">
    <?php  tfuse_shortcode_content('before'); ?>
    <div class="clear"></div>
    </div>
</div>
<?php if ($sidebar_position == 'right') : ?>
         <div id="middle" class="cols2">
<?php endif;
    if ($sidebar_position == 'left') : ?>
         <div id="middle" class="cols2 sidebar_left">
<?php endif;
     if ($sidebar_position == 'full') : ?>
          <div id="middle" class="full_width2">
<?php endif; ?> 
    <div class="container">
        <div class="content" role="main">
            <article class="post-details ">
                <div class="entry clearfix">
                    <div class="service_list postajax clearfix" <?php tfuse_more_pagination();?>>
                        <?php if (have_posts()) 
                         { $count = 0;
                             while (have_posts()) : the_post(); $count++;
                                 get_template_part('listing', 'services');
                                 if ($sidebar_position == 'full' && ($count % 3 == 0)) echo '<div class="clear"></div>';
                                 elseif ($sidebar_position != 'full' && ($count % 2 == 0)) echo '<div class="clear"></div>';
                             endwhile;
                              ?>
                       <?php  } 
                         else 
                         { ?>
                             <h5><?php _e('Sorry, no posts matched your criteria.', 'tfuse'); ?></h5>
                   <?php } ?>
                    </div>
                </div>
                <?php tfuse_load_pagination();?>
                 <?php tfuse_choose_pagination();?>
            </div>
        <?php if (($sidebar_position == 'right') || ($sidebar_position == 'left')) : ?>
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div>
            <?php endif; ?>
        <div class="clear"></div>
        </div>
    </div> 
</div><!--/ .middle -->
<div class="clear"></div>
<div class="middle-bottom">
    <div class="container">
        <?php tfuse_shortcode_content('after'); ?>
    </div>
</div>
<?php $id = tfuse_get_service_id();?>
<input type="hidden" value="<?php echo $id; ?>" name="current_cat"  />
<input type="hidden" value="service" name="is_this_tax"  />
<?php if (($sidebar_position == 'full')){?>
<input type="hidden" value="no" name="sidebar_exist"  />
<?php }
elseif($sidebar_position != 'full'){?>
<input type="hidden" value="yes" name="sidebar_exist"  />
<?php }?>
<?php get_footer();?>