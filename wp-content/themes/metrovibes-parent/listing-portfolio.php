<?php
/**
 * The template for displaying posts on archive pages.
 * To override this template in a child theme, copy this file 
 * to your child theme's folder.
 *
 * @since MetroVibes 1.0
 */
global $post; $count = 0;
$type = tfuse_options('portf_type');
$attachments = tfuse_get_gallery_images($post->ID,TF_THEME_PREFIX . '_thumbnail_image');
$slider_images = array();
    if ($attachments) {
        foreach ($attachments as $attachment){
            $slider_images[] = array(
                'order'        =>$attachment->menu_order,
                'img_full'    => $attachment->guid,
            );
        }
    }
?>
<?php $slider_images = tfuse_aasort($slider_images,'order'); ?>    
<div class="gallery-item clearfix">
    <div class="gallery-image">
        <?php 
            foreach($slider_images as $slider): $count++;
                if($count == 1){ 
                    if($type == 'pretty'): ?>
                        <a href="<?php echo $slider['img_full'];?>" class="preload" id="ajax<?php echo$post->ID;?>" data-rel="prettyPhoto[<?php echo $post->ID ?>]" title="<?php tfuse_custom_title(); ?>">
                            <img src="<?php echo TF_GET_IMAGE::get_src_link($slider['img_full'], 300, 300); ?>" alt="">
                        </a> 
                    <?php endif;
                    if($type == 'single'): ?>
                        <a href="<?php the_permalink(); ?>" class="preload" id="ajax<?php echo$post->ID;?>" title="<?php tfuse_custom_title(); ?>">
                            <img src="<?php echo TF_GET_IMAGE::get_src_link($slider['img_full'], 300, 300); ?>" alt="">
                        </a> 
                    <?php endif;?>
               <?php }
                else { ?>
                     <a href="<?php echo $slider['img_full'];?>" class="hidden" data-rel="prettyPhoto[<?php echo $post->ID ?>]"></a>
              <?php  }
              if($type == 'single' && $count == 1) break;
            endforeach;?>
    </div>
</div>
