<?php 
global $is_tf_blog_page,$post;
$id_post = $post->ID; 
if(tfuse_options('blog_page') != 0 && $id_post == tfuse_options('blog_page')) $is_tf_blog_page = true;
get_header();
if ($is_tf_blog_page) die(); 
?>
<?php $sidebar_position = tfuse_sidebar_position(); ?>
<div class="header-bottom">
    <div class="container">
    <?php  tfuse_shortcode_content('before'); ?>
    <div class="clear"></div>
    </div>
</div>
<?php if ($sidebar_position == 'right') : ?>
         <div id="middle" class="cols2">
<?php endif;
    if ($sidebar_position == 'left') : ?>
         <div id="middle" class="cols2 sidebar_left">
<?php endif;
     if ($sidebar_position == 'full') : ?>
          <div id="middle" class="full_width">
<?php endif; ?> 
    <div class="container">
        <div class="content" role="main">
            <?php  while ( have_posts() ) : the_post();?>
                    <article class="post-details">  
                        <div class="entry clearfix">
                           <?php tfuse_page_custom_title();
                             the_content(); ?>
                        </div><!--/ .entry -->
                    </article>
                    <div class="clear"></div>
            <?php break; endwhile; // end of the loop. ?>
                <?php tfuse_comments(); ?>
        </div>            <?php if (($sidebar_position == 'right') || ($sidebar_position == 'left')) : ?>
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div><!--/ .sidebar -->
            <?php endif; ?>
    </div> 
</div><!--/ .middle -->
<div class="clear"></div>
<div class="middle-bottom">
    <div class="container">
        <?php tfuse_shortcode_content('after'); ?>
    </div>
</div>
<?php get_footer();?>