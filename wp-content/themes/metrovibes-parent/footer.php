<?php
    $home = tfuse_is_homepage();
    $cat_ids = tfuse_get_categories_ids();
    $allhome = tfuse_select_all_home();
    $allblog = tfuse_select_all_blog();
?>
<input type="hidden" value="<?php echo $home; ?>" name="homepage"  />
<input type="hidden" value="<?php echo $allhome; ?>" name="allhome"  />
<input type="hidden" value="<?php echo $allblog; ?>" name="allblog"  />
<input type="hidden" value="<?php echo $cat_ids; ?>" name="categories_ids"  />
<footer <?php echo tfuse_change_footer_bg();?>>
    <div class="container clearfix">
        <?php tfuse_footer();?>
    </div>
</footer>
<!--/ footer -->
<?php wp_footer(); ?>
</div>
</body>
</html>