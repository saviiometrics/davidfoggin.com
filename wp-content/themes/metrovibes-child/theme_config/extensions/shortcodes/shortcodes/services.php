<?php

function tfuse_services($atts, $content = null) {
    extract(shortcode_atts(array( 'order' => '','number' => ''), $atts));

    $output = '';

    if (!empty($order) && ($order == 'ASC' || $order == 'DESC'))
        $order = '&order=' . $order;
    else
        $order = '&orderby=rand';

    $posts = get_posts('post_type=service&posts_per_page=-1' . $order);
    $output .='<div class="service_list clearfix">';
    $k = 0;
    if(!empty($posts))
    {
        foreach ($posts as $pos) {  
            if($k == $number) break; $k++;
            $service_link = tfuse_options('service_link');
            if($service_link != 'link') $link = get_permalink($pos->ID);
            else $link = tfuse_page_options('service_custom_link','',$pos->ID);
                $img = tfuse_page_options('thumbnail_image','',$pos->ID);
                if(!empty($img)) $img = '<img src="'.$img.'" width="112" height="112" alt="">';
               
                $output .= '<div class="service_item">
                                <div class="service_img">'.$img.'</div>
                                 <div class="service_title"><h2>'.$pos->post_title.'</h2></div>
                                <div class="service_desc">
                                    <p>'.strip_tags(apply_filters('the_content',$pos->post_content)).'</p>
                                </div>
                                <div class="service_meta_bot"><a href="'.$link.'" class="link-more">'. __('Find out more...','tfuse').'</a></div>
                            </div>';
            } // End WHILE Loop
    $output .='</div>';
    }
    return $output;
}

$atts = array(
    'name' => __('Services', 'tfuse'),
    'desc' => __('Here comes some lorem ipsum description for the box shortcode.', 'tfuse'),
    'category' => 20,
    'options' => array(
        array(
            'name' => __('Order', 'tfuse'),
            'desc' => __('Select display order', 'tfuse'),
            'id' => 'tf_shc_services_order',
            'value' => 'ASC',
            'options' => array(
                'RAND' => __('Random', 'tfuse'),
                'ASC' => __('Ascending', 'tfuse'),
                'DESC' => __('Descending', 'tfuse')
            ),
            'type' => 'select'
        ),
         array(
            'name' => __('Number of items', 'tfuse'),
            'desc' => __('Enter the number of items', 'tfuse'),
            'id' => 'tf_shc_services_number',
            'value' => '6',
            'type' => 'text'
        ),
    )
);

tf_add_shortcode('services', 'tfuse_services', $atts);
